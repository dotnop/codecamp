let peopleSalary = [
  {
    id: "1001",
    firstname: "Luke",
    lastname: "Skywalker",
    company: "Walt Disney",
    salary: "40000"
  },
  {
    id: "1002",
    firstname: "Tony",
    lastname: "Stark",
    company: "Marvel",
    salary: "1000000"
  },
  {
    id: "1003",
    firstname: "Somchai",
    lastname: "Jaidee",
    company: "Love2work",
    salary: "20000"
  },
  {
    id: "1004",
    firstname: "Monkey D",
    lastname: "Luffee",
    company: "One Piece",
    salary: "9000000"
  }
];

let peopleSalary2 = [];
for (let i = 0; i < peopleSalary.length; i++) {
  let obj = peopleSalary[i];
  let obj2 = {};
  for (let key in obj) {
    if (key != "company") {
      if (key == "salary") {
        let baseSalary = parseInt(obj[key]);
        let arrSalary = [];
        for (let j = 0; j < 3; j++) {
          arrSalary.push(baseSalary);
          baseSalary = parseInt(baseSalary * 1.1);
        }
        obj2[key] = arrSalary;
      } else {
        obj2[key] = obj[key];
      }
    }
  }
  peopleSalary2.push(obj2);
}
console.log(peopleSalary2);
