const Koa = require("koa");
const Router = require("koa-router");
const bodyParser = require("koa-bodyparser");
const cors = require('@koa/cors');

const Todo = require("./controller/todo.controller");

let appPort = 4000;

const router = new Router()
  .get("/", Todo.index)
  .get("/todos", Todo.list)
  .get("/todos/:id", Todo.get)
  .post("/todos", Todo.create)
  .patch("/todos/:id", Todo.update)
  .delete("/todos/:id", Todo.remove)
  .put("/todos/:id/complete", Todo.complete)
  .delete("/todos/:id/complete", Todo.incomplete);


const app = new Koa()
  .use(bodyParser())
  .use(cors())
  .use(router.routes())
  .listen(appPort, () => {
    console.log("Todo API server http://localhost:" + appPort);
  });