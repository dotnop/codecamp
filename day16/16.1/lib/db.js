const mysql = require("mysql2/promise");
const { dbConfig } = require("../config/database.js");

const DB = mysql.createPool({
  connectionLimit: 10,
  host: dbConfig.host,
  port: dbConfig.port,
  user: dbConfig.user,
  password: dbConfig.password,
  database: dbConfig.database
});

module.exports = DB;
