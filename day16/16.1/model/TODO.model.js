const db = require("../lib/db");

async function list() {
  const connection = await db.getConnection();
  const [rows] = await connection.execute("SELECT * FROM todo");
  await connection.release();
  return rows;
}

// TEMPLATE LITERAL
// SQL QUERY STRUCTURE

async function create(newtext) {
  const connection = await db.getConnection();
  const [r] = await connection.execute(
    "INSERT INTO `todo` (`id`, `text`) VALUES (NULL, ?)",
    [newtext]
  );
  const [rows] = await connection.execute(
    "SELECT * FROM todo WHERE id = ?",
    [r.insertId]
  );

  await connection.release();

  return { ok: newtext + " inserted!" };
}

async function get(id) {
  const connection = await db.getConnection();
  const [rows] = await connection.execute(
    "SELECT * FROM todo WHERE id = ?",
    [id]
  );

  await connection.release();

  return rows[0];
}

async function update(id, newtext) {
  const connection = await db.getConnection();
  await connection.execute(
    "UPDATE todo SET text = ? WHERE id = ?",
    [newtext, id]
  );

  const [rows] = await connection.execute(
    "SELECT * FROM todo WHERE id = ?",
    [id]
  );

  await connection.release();

  return rows[0];
}

async function remove(id) {
  const connection = await db.getConnection();
  const [rows] = await connection.execute("DELETE FROM todo WHERE id = ?", [id]);

  console.log('Executing:', 'DELETE FROM todo WHERE id =', id)

  console.log('Remove', id, rows)

  await connection.release();
  return { ok: "id:" + id + " remove!" }
}

async function complete(id) {
  const connection = await db.getConnection();
  const [rows] = await connection.execute(
    "UPDATE todo SET completed_at = now() WHERE id = ?",
    [id]
  );

  await connection.release();
  return { ok: "id " + id + " mark to complete" };
}

async function incomplete(id) {
  const connection = await db.getConnection();
  const [rows] = await connection.execute(
    "UPDATE todo SET completed_at = null WHERE id = ?",
    [id]
  );

  await connection.release();
  return { ok: "id " + id + " mark to incomplete" };
}

module.exports = {
  list,
  create,
  get,
  update,
  remove,
  complete,
  incomplete
};
