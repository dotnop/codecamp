const _ = require("lodash");
const todo = require("../model/TODO.model");

function index(ctx) {
  ctx.body = "use post man!";
}

async function list(ctx) {
  ctx.body = await todo.list(ctx);
  console.log('List', ctx.body)
}

async function create(ctx) {
  //input post key must have same name === var
  //{"todo":"todo something"}
  let { text } = ctx.request.body;
  if (!_.isString(text)) {
    ctx.status = 400;
    ctx.body = {
      error: "text must be string"
    };
    return;
  }

  text = text.trim();
  if (text.length === 0 || text.length > 200) {
    ctx.status = 400;
    ctx.body = {
      error: "text length must be 1 to 200"
    };
    return;
  }

  // create todo from text
  ctx.body = await todo.create(text);
}

async function get(ctx) {
  const id = ctx.params.id;
  if (id >= 0) {
    ctx.body = await todo.get(id);
    console.log('Get Todo', ctx.body)
  }
  return;
}

async function update(ctx) {
  //test data id 1
  //{"todo":"todo something"}
  const id = ctx.params.id;
  const { text } = ctx.request.body;
  if (id >= 0) {
    ctx.body = await todo.update(id, text);
  }
  return;
}

async function remove(ctx) {
  const id = ctx.params.id;

  if (id >= 0) {
    ctx.body = await todo.remove(id);
    console.log(ctx.body)
  }
  return;
}

async function complete(ctx) {
  const id = ctx.params.id;
  if (id >= 0) {
    ctx.body = await todo.complete(id);
  }
  return;
}

async function incomplete(ctx) {
  const id = ctx.params.id;
  if (id >= 0) {
    ctx.body = await todo.incomplete(id);
  }
  return;
}

module.exports = {
  index,
  list,
  create,
  get,
  update,
  remove,
  complete,
  incomplete
};
