"use strict";

const Koa = require("Koa");
const Router = require("koa-router");
const serve = require("koa-static");
const session = require("koa-session");
const render = require("koa-ejs");
const path = require("path");
const app = new Koa();
const router = new Router();
const formidable = require("koa2-formidable");
const mysql = require("mysql2/promise");

const pool = mysql.createPool({
  connectionLimit: 10,
  host: "pr.in.th",
  user: "vjqvjhuk_xam",
  password: "ZEXhqbiQD",
  database: "vjqvjhuk_xam"
});

const baseUrl = "http://localhost:3000/";
const public_html = path.join(__dirname, "public");

render(app, {
  root: path.join(__dirname, "view"),
  layout: "template",
  viewExt: "ejs",
  cache: false,
  debug: true
});

app.keys = [
  "rnES65iPhSC5xKxP9vWQHCx680NtzwpdMT66ybtQ86Ll2T1fv6H5evgwyoOvEe9PtcmQYbgUaipRGJZLNGnG0TugAWNkbekVlQh1jU48p0ihQIzjyx6g7c4f"
];
app.use(formidable());

async function insertNewUser(username, password, email) {
  let sql = `INSERT INTO user (username,password,email) VALUES (?,?,?)`;
  let [result, fields] = await pool.query(sql, [
    username,
    stupidHash(password),
    email
  ]);
  return result.insertId;
}

function stupidHash(password) {
  return password + "12345";
}

async function isEmailExisted(email) {
  let sql = `SELECT email FROM user WHERE email = ?`;
  let [result, fields] = await pool.query(sql, [email]);
  if (result[0]) {
    return true;
  } else {
    return false;
  }
}

async function isUsernameExisted(username) {
  let sql = `SELECT username FROM user WHERE username = ?`;
  let [result, fields] = await pool.query(sql, [username]);
  if (result[0]) {
    return true;
  } else {
    return false;
  }
}

async function isPasswordMatched(username, password) {
  let sql = `SELECT password FROM user
    WHERE username = ?`;
  let [result, fields] = await pool.query(sql, [username]);
  if (result[0].password == stupidHash(password)) {
    return true;
  } else {
    return false;
  }
}

async function getUserInfoByUserId(userId) {
  let sql = `SELECT * FROM user WHERE user_id=?`;
  let [result, fields] = await pool.query(sql, [userId]);
  return result[0];
}

async function getUserInfoByUsername(username) {
  console.log(username);
  let sql = `SELECT * FROM user WHERE username=?`;
  let [result, fields] = await pool.query(sql, [username]);
  return result[0];
}

router
  .get("/", async ctx => {
    await ctx.render("register", {
      baseUrl: baseUrl
    });
  })
  .get("/register", async ctx => {
    await ctx.render("register", {
      baseUrl: baseUrl
    });
  })
  .post("/register_completed", async ctx => {
    let emailExisted = false;
    let usernameExisted = false;
    let emailExistedResult = await isEmailExisted(ctx.request.body.email);
    if (emailExistedResult) {
      emailExisted = true;
    }

    let usernameExistedResult = await isUsernameExisted(
      ctx.request.body.username
    );
    console.log("username : ", ctx.request.body.username);
    if (usernameExistedResult) {
      usernameExisted = true;
    }

    let userId;
    if (!emailExisted && !usernameExisted)
      userId = await insertNewUser(
        ctx.request.body.username,
        ctx.request.body.password,
        ctx.request.body.email
      );

    //console.log(result);
    await ctx.render("register_completed", {
      username: ctx.request.body.username,
      email: ctx.request.body.email,
      emailExisted: emailExisted,
      usernameExisted: usernameExisted,
      userId: userId
    });
  })
  .post("/register_completed_ajax", async ctx => {
    let emailExisted = false;
    let usernameExisted = false;
    let emailExistedResult = await isEmailExisted(ctx.request.body.email);
    if (emailExistedResult) {
      emailExisted = true;
    }

    let usernameExistedResult = await isUsernameExisted(
      ctx.request.body.username
    );
    if (usernameExistedResult) {
      usernameExisted = true;
    }

    let userId;
    if (!emailExisted && !usernameExisted)
      userId = await insertNewUser(
        ctx.request.body.username,
        ctx.request.body.password,
        ctx.request.body.email
      );

    ctx.body = {
      emailExisted: emailExisted,
      usernameExisted: usernameExisted
    };
  })
  .get("/login", async function(ctx) {
    await ctx.render("login");
  })
  .post("/login_completed", async function(ctx) {
    let usernameExisted = false;
    let passwordMatched = false;

    let usernameExistedResult = await isUsernameExisted(
      ctx.request.body.username
    );
    if (usernameExistedResult) {
      usernameExisted = true;
      if (
        await isPasswordMatched(
          ctx.request.body.username,
          ctx.request.body.password
        )
      )
        passwordMatched = true;
    }

    if (passwordMatched) {
      let userRow = await getUserInfoByUsername(ctx.request.body.username);
      ctx.session.userId = userRow.user_id;
      await ctx.render("login_completed", {
        username: ctx.request.body.username
      });
    } else {
      await ctx.render("login_failed", {
        usernameExisted: usernameExisted,
        passwordMatched: passwordMatched
      });
    }
  })
  .get("/my_profile", async ctx => {
    const userRow = await getUserInfoByUserId(ctx.session.userId);
    await ctx.render("user_profile", {
      userRow: userRow
    });
  })
  .get("/logout", async ctx => {
    console.log("logout");
    ctx.session = null;
    ctx.body = "Logged out!!";
  });

const sessionStore = {};

const CONFIG = {
  key: "koa:sess" /** (string) cookie key (default is koa:sess) */,
  /** (number || 'session') maxAge in ms (default is 1 days) */
  /** 'session' will result in a cookie that expires when session/browser is closed */
  /** Warning: If a session cookie is stolen, this cookie will never expire */
  maxAge: 60 * 60 * 1000,
  overwrite: true /** (boolean) can overwrite or not (default true) */,
  httpOnly: true /** (boolean) httpOnly or not (default true) */,
  store: {
    get(key, maxAge, { rolling }) {
      return sessionStore[key];
    },
    set(key, sess, maxAge, { rolling, changed }) {
      sessionStore[key] = sess;
    },
    destroy(key) {}
  }
};

app.use(serve(public_html));
app.use(session(CONFIG, app));
app.use(async (ctx, next) => {
  if (ctx.path == "/login_completed" || ctx.path == "/register") {
    await next();
    return;
  }
  if (!ctx.session || (ctx.session && !ctx.session.userId)) {
    await ctx.render("login");
  } else {
    await next();
  }
});

app.use(router.routes());
app.use(router.allowedMethods());
app.listen(3000, () => {
  console.log("open on port 3000");
});
