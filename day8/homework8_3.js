const fs = require("fs");

class Employee {
  constructor(firstname, lastname, salary) {
    this._salary = salary; // simulate private variable
    this.firstname = firstname;
    this.dressCode = "tshirt";
  }
  setSalary(newSalary) {
    this._salary = newSalary;
  }
  getSalary() {
    // simulate public method
    return this._salary;
  }
  work(employee) {
    // leave blank for child class to be overidden
  }
  leaveForVacation(year, month, day) {}
}

class CEO extends Employee {
  constructor(firstname, lastname, salary) {
    super(firstname, lastname, salary);
    this.dressCode = "suit";
    this.employeeRaw = [];
    this.employee = [];
  }

  async getEmployees() {
    try {
      let rawData = await this._getDataFromFile("./homework1.json");
      this.employeeRaw = rawData;
      let result = await this._getEmployee(rawData);
      this.employee = result;
      return result;
    } catch (err) {
      console.error(err);
    }
  }

  // getEmployeesRaw().then(result => {
  //   ceo.employeesRaw = result;
  //   let employees = ceo.employeesRaw;
  //   console.log(employees);
  // });

  _getDataFromFile(fileName) {
    return new Promise((resolve, reject) => {
      fs.readFile(fileName, "utf8", function(err, data) {
        if (err) {
          reject(err);
        } else {
          resolve(JSON.parse(data));
        }
      });
    });
  }

  _getEmployee(rawData) {
    return new Promise((resolve, reject) => {
      let returnData = rawData.map(function(person) {
        let objProgramer = new Programmer(
          person.id,
          person.firstname,
          person.lastname,
          person.salary
        );
        return objProgramer;
      });
      if (returnData.lenght == 0) {
        reject("error");
      } else {
        resolve(returnData);
      }
    });
  }

  getSalary() {
    return super.getSalary() * 2;
  }

  work(employee) {
    // simulate public method
    this._fire(employee);
    this._hire(employee);
    this._seminar();
    this._golf();
  }

  increaseSalary(employee, newSalary) {
    let employeeSalary = employee.getSalary();
    if (employeeSalary > newSalary) {
      console.log(employee.firstname + "'s salary is less than before!!");
    } else {
      this.setSalary(newSalary);
      console.log(
        employee.firstname + "'s salary has been set to " + newSalary
      );
    }
  }

  gossip(employee, context) {
    console.log("Hey " + employee.firstname + ", " + context);
  }

  _fire(employee) {
    console.log(
      employee.firstname + " has been fired! Dress with :" + employee.dressCode
    );
  }

  _hire(employee) {
    console.log(
      employee.firstname +
        " has been hired back! Dress with :" +
        employee.dressCode
    );
  }

  _seminar() {
    console.log("He is going to seminar Dress with :" + this.dressCode);
  }

  _golf() {
    // simulate private method
    this.dressCode = "golf_dress";
    console.log(
      "He goes to golf club to find a new connection." +
        " Dress with :" +
        this.dressCode
    );
  }
}

class Programmer {
  constructor(id, firstName, lastName, salary) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.salary = salary;
  }
  work() {
    console.log("working");
  }
  summer() {
    console.log("summer");
  }
  gossip() {
    console.log("gossip");
  }
}

let dang = new Employee("Dang", "Red", 10000);
let ceo = new CEO("Somchai", "Sudlor", 30000);

ceo.getEmployees().then(x => {
  let employee = ceo.employee;
  console.log(employee);
});
