class MobilePhone {
  PhoneCall()
  SMS()
  InternetSurfing()
}

class Samsung_Galaxy_S8 extends MobilePhone {
  GooglePlay()
  TransformToPC()
}

class SamSung_Galaxy_Note_8 extends Samsung_Galaxy_S8 {
  UsePen()
}

class iPhone extends MobilePhone {
  AppStore()
}

class iPhoneX extends iPhone {
  FaceTime()
}

class iPhone8 extends iPhone {
  TouchID()
}
