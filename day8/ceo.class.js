class Employee {
  constructor(firstname, lastname, salary) {
    this._salary = salary; // simulate private variable
    this.firstname = firstname;
    this.dressCode = "tshirt";
  }
  setSalary(newSalary) {
    this._salary = newSalary;
  }
  getSalary() {
    // simulate public method
    return this._salary;
  }
  work(employee) {
    // leave blank for child class to be overidden
  }
  leaveForVacation(year, month, day) {}
}

class CEO extends Employee {
  constructor(firstname, lastname, salary) {
    super(firstname, lastname, salary);
    this.dressCode = "suit";
  }   

  getSalary() {
    // simulate public method
    return super.getSalary() * 2;
  }
  work(employee) {
    // simulate public method
    this._fire(employee);
    this._hire(employee);
    this._seminar();
    this._golf();
  }
  increaseSalary(employee, newSalary) {
    let employeeSalary = employee.getSalary();
    if (employeeSalary > newSalary) {
      console.log(employee.firstname + "'s salary is less than before!!");
    } else {
      this.setSalary(newSalary);
      console.log(
        employee.firstname + "'s salary has been set to " + newSalary
      );
    }
  }

  gossip(employee, context) {
    console.log("Hey " + employee.firstname + ", " + context);
  }
  _fire(employee) {
    console.log(
      employee.firstname + " has been fired! Dress with :" + employee.dressCode
    );
  }
  _hire(employee) {
    console.log(
      employee.firstname +
        " has been hired back! Dress with :" +
        employee.dressCode
    );
  }
  _seminar() {
    console.log("He is going to seminar Dress with :" + this.dressCode);
  }
  _golf() {
    // simulate private method
    this.dressCode = "golf_dress";
    console.log(
      "He goes to golf club to find a new connection." +
        " Dress with :" +
        this.dressCode
    );
  }
}

exports.CEO = CEO;
exports.Employee = Employee;
