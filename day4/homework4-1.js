let arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

function findOdd(number) {
  if (number % 2 === 0) {
    return number;
  }
}

function multiple1000(number) {
  return number * 1000;
}

let newArr = arr.filter(findOdd).map(multiple1000);

console.log(newArr);
