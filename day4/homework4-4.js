const fs = require("fs");

function filterMale(person) {
  return person.gender == "male";
}

function filterFriends(person) {
  return person.friends.length >= 2;
}

function filterFeilds(person) {
  let personNewData = {};
  let feildAllow = ["name", "gender", "company", "email", "friends", "balance"];
  feildAllow.map(key => (personNewData[key] = person[key]));
  return personNewData;
}

function mapBalance(person) {
  person.balance =
    "$" + (Number(person.balance.replace(/[^0-9\.-]+/g, "")) / 10).toFixed(2);
  return person;
}

function buildSelectedPersonData(data) {
  let info = JSON.parse(data);
  let selectPerson = info.map(filterFeilds);
  return selectPerson
    .filter(filterMale)
    .filter(filterFriends)
    .map(mapBalance);
}

fs.readFile(__dirname + "/socialdata.txt", "utf8", (err, data) => {
  if (err) console.error(err);
  else {
    let selectedpersonTxt = JSON.stringify(buildSelectedPersonData(data));
    fs.writeFile(__dirname + "/selectedperson.json", selectedpersonTxt, err => {
      if (err) console.error(err);
      else console.log("write file sucess");
    });
  }
});
