const fs = require("fs");
let info;
let newSalary = [1.1, 1.21, 1.331];
let salaryAdjusted = [];

function showFullName(data) {
  data.map(person => {
    person.fullname = person.firstname + " " + person.lastname;
    salaryAdjusted = newSalary.map(rate => person.salary * rate);
    person.salary = salaryAdjusted;
    console.log(person.firstname + " " + person.lastname);
  });
  return data;
}

fs.readFile(__dirname + "/homework1.json", "utf8", (err, data) => {
  if (err) {
    console.error(err);
  } else {
    //console.log(data)
    info = JSON.parse(data);
    info = showFullName(info);
    console.log(info);
  }
});
