const fs = require("fs");

function findPerson(person) {
  let personSalary = parseInt(person.salary);
  if (personSalary < 100000) {
    person.salary = personSalary * 2;
    return person;
  }
}

function calSalary(person) {
  return parseInt(person.salary);
}

function sumSalaries(sum, personSalary) {
  return sum + personSalary;
}

fs.readFile(__dirname + "/homework1.json", "utf8", function(err, data) {
  if (err) {
    console.error(err);
  } else {
    let allInfo = JSON.parse(data);
    let info = allInfo.filter(findPerson);
    const sumSalary = allInfo.map(calSalary).reduce(sumSalaries);
    console.log(info);
    console.log(sumSalary);
  }
});
