-- create database
create  database bookstore;

-- create table
CREATE TABLE IF NOT EXISTS `employees`(
    `id` int auto_increment,
    `firstname` varchar(255) NOT NULL,
    `lastname` varchar(255) NOT NULL,
    `age` int(2),
    `dateadd` timestamp default now() NOT NULL,
    primary key (`id`),
    UNIQUE KEY `fullname` (`firstname`,`lastname`)
) ENGINE=innoDB auto_increment=1 default charset=utf8;

CREATE TABLE IF NOT EXISTS `books`(
    `isbn` varchar(20) NOT NULL,
    `bookname` varchar(255) NOT NULL,
    `price` decimal(8,2) NOT NULL,
    `dateadd` timestamp default now() NOT NULL,
    primary key (`isbn`),
    UNIQUE KEY (`isbn`)
)ENGINE=innoDB default charset=utf8;

CREATE TABLE IF NOT EXISTS `sales`(
    `isbn` varchar(255) NOT NULL,
    `userid` int(8) NOT NULL,
    `price` decimal(8,2) NOT NULL,
    `qty` int(8) NOT NULL,
    `saledate` timestamp default now()
)ENGINE=innoDB default charset=utf8;