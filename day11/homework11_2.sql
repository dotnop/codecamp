-- add 10 person into users table
use `bookstore`;
insert into `employees`
(`firstname`,`lastname`,`age`) 
values
('Anong','Wattana',29),
('Kamon','Arthit',53),
('Klahan','Somboon',39),
('Mali','Sasithorn',28),
('Porntip','Malai',31),
('Prasert','Kanda',32),
('Prasert','Ratree',50),
('Sasithorn','Mongkut',44),
('Somboon','Pakpao',24),
('Somchai','Sunan',55),
('Somporn','Malai',46),
('Somsak','Malai',41),
('Thaksin','Sunan',37);

-- delete person id = 5
delete from `employees` 
where `id` =5;

-- add feild to user table
alter  table `employees`
add column address varchar(255) default NULL
AFTER `age`;

-- count all users
select count(*) as `sum` from `employees`;

-- filter who has age more than 20
select `firstname`,`lastname` from `employees`
where `age` > 20;