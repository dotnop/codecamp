-- after import mysql from 
-- curl -LO https://github.com/datacharmer/test_db/archive/master.zip
-- $ unzip test_db-master.zip
-- $ cd test_db-master
-- $ mysql -u root < employees.sql

-- หาชื่อ database ที่ import
show databases;
-- +--------------------+
-- | Database           |
-- +--------------------+
-- | bookstore          |
-- | codecamp           |
-- | db1                |
-- | employees          |
-- | information_schema |
-- | mysql              |
-- | performance_schema |
-- | phpmyadmin         |
-- | test               |
-- +--------------------+
-- 9 rows in set (0.00 sec)

-- หาชื่อ tables ทั้งหมดที่ import
use employees
show tables;
-- +----------------------+
-- | Tables_in_employees  |
-- +----------------------+
-- | current_dept_emp     |
-- | departments          |
-- | dept_emp             |
-- | dept_emp_latest_date |
-- | dept_manager         |
-- | employees            |
-- | salaries             |
-- | titles               |
-- +----------------------+
-- 8 rows in set (0.00 sec)


-- ดูโครงสร้างว่าแต่ละ table มี column อะไรบ้าง
describe current_dept_emp;
-- +-----------+---------+------+-----+---------+-------+
-- | Field     | Type    | Null | Key | Default | Extra |
-- +-----------+---------+------+-----+---------+-------+
-- | emp_no    | int(11) | NO   |     | NULL    |       |
-- | dept_no   | char(4) | NO   |     | NULL    |       |
-- | from_date | date    | YES  |     | NULL    |       |
-- | to_date   | date    | YES  |     | NULL    |       |
-- +-----------+---------+------+-----+---------+-------+
-- 4 rows in set (0.03 sec)

describe departments;
-- +-----------+-------------+------+-----+---------+-------+
-- | Field     | Type        | Null | Key | Default | Extra |
-- +-----------+-------------+------+-----+---------+-------+
-- | dept_no   | char(4)     | NO   | PRI | NULL    |       |
-- | dept_name | varchar(40) | NO   | UNI | NULL    |       |
-- +-----------+-------------+------+-----+---------+-------+
-- 2 rows in set (0.01 sec)

describe dept_emp;
-- +-----------+---------+------+-----+---------+-------+
-- | Field     | Type    | Null | Key | Default | Extra |
-- +-----------+---------+------+-----+---------+-------+
-- | emp_no    | int(11) | NO   | PRI | NULL    |       |
-- | dept_no   | char(4) | NO   | PRI | NULL    |       |
-- | from_date | date    | NO   |     | NULL    |       |
-- | to_date   | date    | NO   |     | NULL    |       |
-- +-----------+---------+------+-----+---------+-------+
-- 4 rows in set (0.01 sec)

describe dept_emp_latest_date;
-- +-----------+---------+------+-----+---------+-------+
-- | Field     | Type    | Null | Key | Default | Extra |
-- +-----------+---------+------+-----+---------+-------+
-- | emp_no    | int(11) | NO   |     | NULL    |       |
-- | from_date | date    | YES  |     | NULL    |       |
-- | to_date   | date    | YES  |     | NULL    |       |
-- +-----------+---------+------+-----+---------+-------+
-- 3 rows in set (0.01 sec)

describe dept_manager;
-- +-----------+---------+------+-----+---------+-------+
-- | Field     | Type    | Null | Key | Default | Extra |
-- +-----------+---------+------+-----+---------+-------+
-- | emp_no    | int(11) | NO   | PRI | NULL    |       |
-- | dept_no   | char(4) | NO   | PRI | NULL    |       |
-- | from_date | date    | NO   |     | NULL    |       |
-- | to_date   | date    | NO   |     | NULL    |       |
-- +-----------+---------+------+-----+---------+-------+
-- 4 rows in set (0.01 sec)

describe employees;
-- +------------+---------------+------+-----+---------+-------+
-- | Field      | Type          | Null | Key | Default | Extra |
-- +------------+---------------+------+-----+---------+-------+
-- | emp_no     | int(11)       | NO   | PRI | NULL    |       |
-- | birth_date | date          | NO   |     | NULL    |       |
-- | first_name | varchar(14)   | NO   |     | NULL    |       |
-- | last_name  | varchar(16)   | NO   |     | NULL    |       |
-- | gender     | enum('M','F') | NO   |     | NULL    |       |
-- | hire_date  | date          | NO   |     | NULL    |       |
-- +------------+---------------+------+-----+---------+-------+
-- 6 rows in set (0.02 sec)

describe salaries;
-- +-----------+---------+------+-----+---------+-------+
-- | Field     | Type    | Null | Key | Default | Extra |
-- +-----------+---------+------+-----+---------+-------+
-- | emp_no    | int(11) | NO   | PRI | NULL    |       |
-- | salary    | int(11) | NO   |     | NULL    |       |
-- | from_date | date    | NO   | PRI | NULL    |       |
-- | to_date   | date    | NO   |     | NULL    |       |
-- +-----------+---------+------+-----+---------+-------+
-- 4 rows in set (0.03 sec)

describe titles;
-- +-----------+-------------+------+-----+---------+-------+
-- | Field     | Type        | Null | Key | Default | Extra |
-- +-----------+-------------+------+-----+---------+-------+
-- | emp_no    | int(11)     | NO   | PRI | NULL    |       |
-- | title     | varchar(50) | NO   | PRI | NULL    |       |
-- | from_date | date        | NO   | PRI | NULL    |       |
-- | to_date   | date        | YES  |     | NULL    |       |
-- +-----------+-------------+------+-----+---------+-------+
-- 4 rows in set (0.01 sec)

-- บริษัทนี้มีพนักงานตำแหน่งอะไรบ้าง ?
use employees;
select distinct(`title`) from `titles`;
-- +--------------------+
-- | title              |
-- +--------------------+
-- | Senior Engineer    |
-- | Staff              |
-- | Engineer           |
-- | Senior Staff       |
-- | Assistant Engineer |
-- | Technique Leader   |
-- | Manager            |
-- +--------------------+
-- 7 rows in set (0.28 sec)

-- บริษัทนี้มีพนักงานกี่คน เป็นผู้ชายกี่คน ผู้หญิงกี่คน
select 
(select count(*)  from employees) as employee_qty,
(select count(*)  from employees where gender = 'M' ) as male,
(select count(*)  from employees where gender = 'F') as female
from employees 
limit 1;
-- +--------------+--------+--------+
-- | employee_qty | male   | female |
-- +--------------+--------+--------+
-- |       300024 | 179973 | 120051 |
-- +--------------+--------+--------+
-- 1 row in set (0.31 sec)

select count(distinct(last_name)) as surname_qty from employees;
-- select count(distinct last_name) as surname_qty from employees;
-- +-------------+
-- | surname_qty |
-- +-------------+
-- |        1637 |
-- +-------------+
-- 1 row in set (0.23 sec)