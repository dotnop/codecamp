const Koa = require("koa");
const session = require("koa-session");
const mysql2 = require("mysql2/promise");

let db = null;

function getDB() {
  return mysql2.createConnection({
    host: "localhost",
    user: "root",
    database: "hw17"
  });
}

async function getSessionFromDB(key) {
  let db = await getDB();
  let jsonObj = {};
  try {
    let [result] = await db.execute("SELECT data from sessions where id = ?", [
      key
    ]);
    jsonObj = JSON.parse(result[0].data);
  } catch (error) {
    console.error(error);
  } finally {
    return jsonObj;
  }
}

async function storeSessionToDB(key, sess) {
  let db = await getDB();
  const jsonObj = JSON.stringify(sess);
  try {
    await db.execute("INSERT INTO sessions (id,data) values (?,?)", [
      key,
      jsonObj
    ]);
  } catch (error) {
    console.error(error);
    try {
      await db.execute("UPDATE sessions  SET data = ? WHERE id = ?", [
        jsonObj,
        key
      ]);
    } catch (error) {
      console.error(error);
    }
  }
}

const app = new Koa();

const sessionStore = {};
const sessionConfig = {
  key: "sess",
  maxAge: null,
  httpOnly: true,
  store: {
    get(key, maxAge, { rolling }) {
      return getSessionFromDB(key);
    },
    set(key, sess, maxAge, { rolling }) {
      storeSessionToDB(key, sess);
    },
    destroy(key) {}
  }
};

function handler(ctx) {
  if (ctx.path === "/favicon.ico") {
    return;
  } else {
    console.log("session1:" + ctx.session.views);
    let n = ctx.session.views || 0;
    ctx.session.views = ++n;
    console.log("session2:" + ctx.session.views);
    ctx.body = `${n} views`;
  }
}

app.keys = ["supersecret"];
app
  .use(session(sessionConfig, app))
  .use(handler)
  .listen(3000);
