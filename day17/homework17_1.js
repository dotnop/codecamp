const Koa = require("koa");
const session = require("koa-session");
const mysql2 = require("mysql2/promise");

let db = null;

function getDB() {
  return mysql2.createConnection({
    host: "localhost",
    user: "root",
    database: "hw17"
  });
}

async function storeSessionToDB(key, sess) {
  let db = await getDB();
  const newSessionData = JSON.stringify(sess);
  sess.views++;
  console.log(newSessionData);
  const updateSessionData = JSON.stringify(sess);
  console.log("storeSessionToDB:" + newSessionData);
  await db.execute(
    "INSERT INTO sessions (id,data) VALUES (?,?) ON DUPLICATE KEY UPDATE data = ?", [key, newSessionData, updateSessionData]
  );
  //..wtf 
  let sessionStore[key] = sess
}

async function getSessionFromDB(key) {
  let db = await getDB();
  try {
    let [result] = await db.execute("SELECT sess from  sessions where id = ?", [
      key
    ]);
    let jsonObj = JSON.parse(result);
    jsonObj.views++;
    result = JSON.stringify(jsonObj);
    console.log("getSessionFromDB :" + result);
    // return result['sess'];
  } catch (error) {
    return;
  }
}

async function destroySessionInDB(key) {
  let db = await getDB();
  try {
    await db.execute("Delete from sessions where id = ?", [key]);
  } catch (error) {
    return;
  }
}

const sessionStore = {}
const sessionConfig = {
  key: "sess",
  maxAge: 3600 * 1000,
  httpOnly: true,
  store: {
    async get(key, MaxAge, {
      rolling
    }) {
      console.log("get db key:" + key);
      return await getSessionFromDB(key);
      //return sessionStore[key];
    },
    async set(key, sess, maxAge, {
      rolling
    }) {
      console.log("set db key:" + key);
      //sessionStore[key] = sess;
      await storeSessionToDB(key, sess);
    },
    async destroy(key) {
      await destroySessionInDB(key);
    }
  }
};

const app = new Koa();

app.keys = ["sepersecret"];

function handler(ctx) {
  if (ctx.path === "/favicon.ico") return;
  let n = ctx.session.views || 0;
  ctx.session.views = ++n;
  ctx.body = n + " views";
  console.log(ctx.session);
  //   console.log("sessionStore:"+sessionStore);
}

app
  .use(session(sessionConfig, app))
  .use(handler)
  .listen(4000, () => {
    console.log("listening on port 4000");
  });