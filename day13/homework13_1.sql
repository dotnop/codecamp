use lab12;
-- จากคอร์สที่มีคนเรียนทั้งหมด ได้เงินเท่าไร
select 
sum(c.price) as allPaid 
from enrolls e 
inner join courses c on c.id = e.course_id;
-- +---------+
-- | allPaid |
-- +---------+
-- |    4645 |
-- +---------+
-- 1 row in set (0.00 sec)

-- นักเรียนแต่ละคน ซื้อคอร์สไปคนละเท่าไร
select 
s.name as student_name,
sum(c.price) as paid
from enrolls e
inner join courses c on c.id = e.course_id
inner join students s on s.id = e.student_id
group by s.id
order by s.name;
-- +---------------------------+------+
-- | student_name              | paid |
-- +---------------------------+------+
-- | Chairat Janpong           |   95 |
-- | Chao-Khun-Sa Vanich       |  570 |
-- | Komn Udomprecha           |  500 |
-- | Mok Udomprecha            |  840 |
-- | Nakarin Pongchandaj       |  390 |
-- | Prachuab Noppachorn       |  470 |
-- | Sarut Mahidol             |  320 |
-- | Somdet-ong-yai Nakpradith |  740 |
-- | Thampon Dajpaisarn        |  630 |
-- | Thong Di Chutimant        |   90 |
-- +---------------------------+------+
-- 10 rows in set (0.00 sec)