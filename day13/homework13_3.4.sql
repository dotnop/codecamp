-- prepare data
DROP DATABASE `hw13`;
CREATE DATABASE `hw13`;
CREATE TABLE `hw13`.`accounts` ( 
    `id` INT NOT NULL AUTO_INCREMENT,
    `balance` DECIMAL(10,2) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB CHARSET=utf8 COLLATE utf8_general_ci;
USE `hw13`;
INSERT INTO `accounts` (`balance`) VALUES ('200');
SELECT * FROM `accounts` WHERE 1;
-- +----+---------+
-- | id | balance |
-- +----+---------+
-- |  1 |  200.00 |
-- +----+---------+
-- 1 row in set (0.00 sec)

-- +----------------+------------+---------------------+---------------+
-- |    level       |Dirty reads |Non-repeatable reads |Phantoms reads |
-- +----------------+------------+---------------------+---------------+
-- |READ UNCOMMITTED| มีโอกาส     |มีโอกาส                |มีโอกาส         |
-- |READ COMMITTED  | ไม่มี        |มีโอกาส                |มีโอกาส         |
-- |REPEATABLE READ | ไม่มี        |ไม่มี                  |มีโอกาส             |
-- |SERIALIZABLE    | ไม่มี        |ไม่มี                  |ไม่มี             |
-- +----------------+------------+---------------------+---------------+

-- -------------------------------------------------------------------------
-- SERIALIZABLE && Dirty reads
-- terminal [1]
use hw13;
set transaction isolation level serializable;
begin;
-- terminal [2]
use hw13;
set transaction isolation level serializable;
begin;
-- terminal [1]
select balance from accounts where id = 1;
-- +---------+
-- | balance |
-- +---------+
-- |  200.00 |
-- +---------+
-- 1 row in set (0.00 sec)
-- terminal [2]
update accounts set balance = 1000 where id = 1;
-- *** process waiting *** ok pass
-- terminal [1]
select balance from accounts where id = 1;
-- +---------+
-- | balance |
-- +---------+
-- |  200.00 | ** ok pass
-- +---------+
-- 1 row in set (0.00 sec)
-- terminal [2]
rollback;
-- terminal [1]
select balance from accounts where id = 1;
-- +---------+
-- | balance |
-- +---------+
-- | 1000.00 | ** shold be ok 
-- +---------+
-- 1 row in set (0.00 sec)
-- SERIALIZABLE && Dirty reads = ไม่มี
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
-- SERIALIZABLE && Non-repeatable reads
-- terminal [1]
use hw13;
set transaction isolation level serializable;
begin;
-- terminal [2]
use hw13;
set transaction isolation level serializable;
begin;
-- terminal [1]
select balance from accounts where id = 1;
-- +---------+
-- | balance |
-- +---------+
-- |  200.00 |
-- +---------+
-- 1 row in set (0.00 sec)
-- terminal [2]
update accounts set balance = 1000 where id = 1;
-- *** terminal freezzzz It's ok
commit;
-- terminal [1]
select balance from accounts where id = 1;
-- +---------+
-- | balance |
-- +---------+
-- |  200.00 | ** 200 ok
-- +---------+
-- 1 row in set (0.00 sec)
-- SERIALIZABLE && Non-repeatable reads = ไม่มี
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
-- SERIALIZABLE && Phantoms reads
-- terminal [1]
use hw13;
set transaction isolation level serializable;
begin;
-- terminal [2]
use hw13;
set transaction isolation level serializable;
begin;
-- terminal [1]
select count(*) from accounts;
-- +----------+
-- | count(*) |
-- +----------+
-- |        1 |
-- +----------+
-- 1 row in set (0.00 sec)
-- terminal [2]
insert into accounts (id, balance) values (2, 100);
-- terminal freezzzz it's OK
select count(*) from accounts;
-- +----------+
-- | count(*) |
-- +----------+
-- |        2 |
-- +----------+
-- 1 row in set (0.00 sec)
commit;
-- terminal [1]
select count(*) from accounts;
-- +----------+
-- | count(*) |
-- +----------+
-- |        1 | ** ok
-- +----------+
-- 1 row in set (0.00 sec)
-- SERIALIZABLE && Phantoms reads = ไม่มี
-- -------------------------------------------------------------------------