use lab12;
-- จากการบ้านที่ 1 ข้อ 2 นักเรียนแต่ละคน ซื้อคอร์สไปคนละเท่าไร จำนวนกี่คอร์ส (ให้ใช้แค่
-- statement เดียวเท่านั้น)
select 
s.name AS student_name,
count(e.course_id) AS course_qty
FROM enrolls e
INNER JOIN courses c ON c.id = e.course_id
INNER JOIN students s ON s.id = e.student_id
GROUP BY s.id
ORDER BY s.name;
-- +---------------------------+------------+
-- | student_name              | course_qty |
-- +---------------------------+------------+
-- | Chairat Janpong           |          2 |
-- | Chao-Khun-Sa Vanich       |          7 |
-- | Komn Udomprecha           |          7 |
-- | Mok Udomprecha            |         10 |
-- | Nakarin Pongchandaj       |          5 |
-- | Prachuab Noppachorn       |          6 |
-- | Sarut Mahidol             |          5 |
-- | Somdet-ong-yai Nakpradith |          9 |
-- | Thampon Dajpaisarn        |          7 |
-- | Thong Di Chutimant        |          1 |
-- +---------------------------+------------+
-- 10 rows in set (0.00 sec)

-- นักเรียนแต่ละคน ซื้อคอร์สไหน ราคาแพงสุด
use lab12;
-- ข้อมูลทั้งหมดทุกคน
create view allprice AS
select
    s1.id,
    s1.name student_name, 
    c1.name course_name, 
    c1.price price
from
    students s1
INNER JOIN enrolls AS e1 ON s1.id = e1.student_id
INNER JOIN courses AS c1 ON c1.id = e1.course_id
ORDER BY s1.id;

-- ข้อมูลราคาสูงสุดของแต่ละคนย
create view maxprice AS
select
    s2.id,
    max(c2.price) max_price
from
    students s2
INNER JOIN enrolls AS e2 ON s2.id = e2.student_id
INNER JOIN courses AS c2 ON c2.id = e2.course_id
GROUP BY s2.id 
ORDER BY s2.id;

-- MAX
select 
    distinct(a1.student_name),  
    a1.course_name, 
    a1.price 
FROM 
    allprice a1
left join maxprice AS m1 ON m1.max_price = a1.price
where a1.price = m1.max_price;

-- +---------------------------+-------------------------------------+-------+
-- | student_name              | course_name                         | price |
-- +---------------------------+-------------------------------------+-------+
-- | Chairat Janpong           | OWASP Top 10                        |    75 |
-- | Mok Udomprecha            | Writing                             |    90 |
-- | Mok Udomprecha            | Design and Architecture             |    90 |
-- | Mok Udomprecha            | Country Music                       |    90 |
-- | Mok Udomprecha            | Comedy                              |    90 |
-- | Mok Udomprecha            | Cooking                             |    90 |
-- | Mok Udomprecha            | Building a Fashion Brand            |    90 |
-- | Mok Udomprecha            | Singing                             |    90 |
-- | Mok Udomprecha            | Fashion Design                      |    90 |
-- | Mok Udomprecha            | Electronic Music Production         |    90 |
-- | Somdet-ong-yai Nakpradith | Cooking                             |    90 |
-- | Somdet-ong-yai Nakpradith | Conservation                        |    90 |
-- | Somdet-ong-yai Nakpradith | Design and Architecture             |    90 |
-- | Somdet-ong-yai Nakpradith | Country Music                       |    90 |
-- | Somdet-ong-yai Nakpradith | Acting                              |    90 |
-- | Somdet-ong-yai Nakpradith | Tennis                              |    90 |
-- | Somdet-ong-yai Nakpradith | Singing                             |    90 |
-- | Somdet-ong-yai Nakpradith | Screenwriting                       |    90 |
-- | Komn Udomprecha           | Conservation                        |    90 |
-- | Komn Udomprecha           | Building a Fashion Brand            |    90 |
-- | Komn Udomprecha           | Shooting, Ball Handler, and Scoring |    90 |
-- | Komn Udomprecha           | Writing #2                          |    90 |
-- | Komn Udomprecha           | Singing                             |    90 |
-- | Nakarin Pongchandaj       | Acting                              |    90 |
-- | Nakarin Pongchandaj       | Jazz                                |    90 |
-- | Nakarin Pongchandaj       | Cooking                             |    90 |
-- | Nakarin Pongchandaj       | Writing #2                          |    90 |
-- | Sarut Mahidol             | Conservation                        |    90 |
-- | Sarut Mahidol             | Writing                             |    90 |
-- | Sarut Mahidol             | Cooking                             |    90 |
-- | Prachuab Noppachorn       | Writing                             |    90 |
-- | Prachuab Noppachorn       | Singing                             |    90 |
-- | Prachuab Noppachorn       | Cooking                             |    90 |
-- | Prachuab Noppachorn       | Tennis                              |    90 |
-- | Prachuab Noppachorn       | Cooking #2                          |    90 |
-- | Thampon Dajpaisarn        | Conservation                        |    90 |
-- | Thampon Dajpaisarn        | Singing                             |    90 |
-- | Thampon Dajpaisarn        | Comedy                              |    90 |
-- | Thampon Dajpaisarn        | Shooting, Ball Handler, and Scoring |    90 |
-- | Thampon Dajpaisarn        | Tennis                              |    90 |
-- | Thampon Dajpaisarn        | Jazz                                |    90 |
-- | Thampon Dajpaisarn        | Filmmaking                          |    90 |
-- | Chao-Khun-Sa Vanich       | Acting                              |    90 |
-- | Chao-Khun-Sa Vanich       | Country Music                       |    90 |
-- | Chao-Khun-Sa Vanich       | Shooting, Ball Handler, and Scoring |    90 |
-- | Chao-Khun-Sa Vanich       | Cooking                             |    90 |
-- | Chao-Khun-Sa Vanich       | Writing                             |    90 |
-- | Chao-Khun-Sa Vanich       | Comedy                              |    90 |
-- | Thong Di Chutimant        | Cooking                             |    90 |
-- +---------------------------+-------------------------------------+-------+
-- 49 rows in set (0.00 sec)

-- ข้อมูลราคาต่ำสุดของแต่ละคนย
-- create view minprice AS
-- select
--     s3.id,
--     min(c3.price) min_price
-- from
--     students s3
-- INNER JOIN enrolls AS e3 ON s3.id = e3.student_id
-- INNER JOIN courses AS c3 ON c3.id = e3.course_id
-- GROUP BY s3.id 
-- ORDER BY s3.id;

-- MIN
-- select 
--     distinct(a2.student_name),
--     a2.course_name, 
--     a2.price 
-- FROM 
--     allprice a2
-- left join minprice AS m2 ON m2.min_price = a2.price
-- where a2.price = m2.min_price;