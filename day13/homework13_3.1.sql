-- prepare data
DROP DATABASE `hw13`;
CREATE DATABASE `hw13`;
CREATE TABLE `hw13`.`accounts` ( 
    `id` INT NOT NULL AUTO_INCREMENT,
    `balance` DECIMAL(10,2) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB CHARSET=utf8 COLLATE utf8_general_ci;
USE `hw13`;
INSERT INTO `accounts` (`balance`) VALUES ('200');
SELECT * FROM `accounts` WHERE 1;
-- +----+---------+
-- | id | balance |
-- +----+---------+
-- |  1 |  200.00 |
-- +----+---------+
-- 1 row in set (0.00 sec)

-- +----------------+------------+---------------------+---------------+
-- |    level       |Dirty reads |Non-repeatable reads |Phantoms reads |
-- +----------------+------------+---------------------+---------------+
-- |READ UNCOMMITTED| มีโอกาส     |มีโอกาส                |มีโอกาส         |
-- |READ COMMITTED  | ไม่มี        |มีโอกาส                |มีโอกาส         |
-- |REPEATABLE READ | ไม่มี        |ไม่มี                  |มีโอกาส             |
-- |SERIALIZABLE    | ไม่มี        |ไม่มี                  |ไม่มี             |
-- +----------------+------------+---------------------+---------------+

-- -------------------------------------------------------------------------
-- READ UNCOMMITTED && Dirty reads
-- terminal [1]
use hw13;
set transaction isolation level read uncommitted;
begin;
-- terminal [2]
use hw13;
set transaction isolation level read uncommitted;
begin;
-- terminal [1]
select balance from accounts where id = 1;
-- terminal [2]
update accounts set balance = 1000 where id = 1;
-- terminal [1]
select balance from accounts where id = 1;
-- terminal [2]
rollback;
-- terminal [1]
select balance from accounts where id = 1;
rollback;
-- READ UNCOMMITTED && Dirty reads = มีโอกาส
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
-- READ UNCOMMITTED && Non-repeatable reads
-- terminal [1]
use hw13;
set transaction isolation level read uncommitted;
begin;
-- terminal [2]
use hw13;
set transaction isolation level read uncommitted;
begin;
-- terminal [1]
select balance from accounts where id = 1;
-- +---------+
-- | balance |
-- +---------+
-- |  200.00 |
-- +---------+
-- 1 row in set (0.00 sec)
-- terminal [2]
update accounts set balance = 1000 where id = 1;
commit;
-- terminal [1]
select balance from accounts where id = 1;
-- +---------+
-- | balance |
-- +---------+
-- | 1000.00 | ***ควรเป็น 200
-- +---------+
-- 1 row in set (0.00 sec)
-- READ UNCOMMITTED && Non-repeatable reads = มีโอกาส
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
-- READ UNCOMMITTED && Phantoms reads
-- terminal [1]
use hw13;
set transaction isolation level read uncommitted;
begin;
-- terminal [2]
use hw13;
set transaction isolation level read uncommitted;
begin;
-- terminal [1]
select count(*) from accounts;
-- +----------+
-- | count(*) |
-- +----------+
-- |        1 |
-- +----------+
-- 1 row in set (0.00 sec)
-- terminal [2]
insert into accounts (id, balance) values
(2, 100);
select count(*) from accounts;
-- +----------+
-- | count(*) |
-- +----------+
-- |        2 |
-- +----------+
-- 1 row in set (0.00 sec)
commit;
-- terminal [1]
select count(*) from accounts;
-- +----------+
-- | count(*) |
-- +----------+
-- |        2 | *** ควรเป็น 1
-- +----------+
-- 1 row in set (0.00 sec)
-- READ UNCOMMITTED && Phantoms reads = มีโอกาส
-- -------------------------------------------------------------------------