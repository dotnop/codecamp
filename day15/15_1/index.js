const mysql2 = require("mysql2/promise");
const createUserModel = require("./model/user");

async function main() {
  const db = await mysql2.createConnection({
    host: "localhost",
    user: "root",
    database: "hw15"
  });

  //   let [result] = await db.execute("select * from users");
  //   console.log(result);

  const User = createUserModel(db);

  //   const user1 = await User.find(10);
  //   user1.firstName = "tester";
  //   await user1.save();

  //   const user2 = await User.find(10);
  //   await user2.remove();

  const user3 = await User.findByUsername("u4");
  user3.firstName = "new_name";
  user3.lastName = "new_surname";
  await user3.save();
}

main();
