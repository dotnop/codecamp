class User {
  constructor(db, row) {
    this._db = db;

    this.id = row.id;
    this.firstName = row.first_name;
    this.lastName = row.last_name;
    this.userName = row.username;
  }

  async save() {
    if (!this.userName) {
      this.userName = this.firstName + this.lastName;
    }
    if (!this.lastName) {
      this.lastName = "untitled";
    }
    if (!this.id) {
      const result = await this._db.execute(
        `
          insert into users (
            first_name, last_name , username
          ) values (
            ?, ? , ?
          )
        `,
        [this.firstName, this.lastName, this.userName]
      );

      this.id = result.insertId;
      return;
    }

    return this._db.execute(
      `
        update users
        set
          first_name = ?,
          last_name = ?
        where id = ?
      `,
      [this.firstName, this.lastName, this.id]
    );
  }

  remove() {
    return this._db.execute(
      `
        delete from users where id = ?
      `,
      [this.id]
    );
  }
}

module.exports = function(db) {
  return {
    async find(id) {
      // TODO: fix bug id undefined
      const [rows] = await db.execute(
        `
          select
            id , first_name, last_name , username
          from users
          where id = ?
        `,
        [id]
      );
      return new User(db, rows[0]);
    },
    async findAll() {
      // TODO: fix bug id undefined
      const [rows] = await db.execute(`
          select
          id , first_name, last_name , username
          from users
        `);
      return rows.map(row => new User(db, row));
    },
    async findByUsername(username) {
      const [rows] = await db.execute(
        `select
                id , first_name, last_name , username
              from users
              where username = ?
            `,
        [username]
      );
      return new User(db, rows[0]);
    }
  };
};
