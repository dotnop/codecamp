function createEntity(row) {
  return {
    id: row.id,
    firstName: row.first_name,
    lastName: row.last_name,
    userName: row.username
  };
}

async function find(db, id) {
  const [rows] = await db.execute(
    `select id,first_name, last_name,username
    from users where id = ?`,
    [id]
  );
  return createEntity(rows[0]);
}

async function findAll(db) {
  const [rows] = await db.execute(
    `
    select
    id,first_name, last_name,username
    from users
  `,
    []
  );
  return rows.map(createEntity);
}

async function findByUsername(db, username) {
  const [rows] = await db.execute(
    `
    select
    id,first_name, last_name,username
    from users
    where username = ?
  `,
    [username]
  );
  return createEntity(rows[0]);
}

async function store(db, user) {
  if (!user.lastName) {
    user.lastName = "no surname";
  }
  if (!user.username) {
    user.username = Math.random()
      .toString(36)
      .replace(/[^a-z]+/g, "")
      .substr(0, 8);
  }
  if (!user.id) {
    const result = await db.execute(
      `insert into users (first_name, last_name, username) values (?, ?, ?)
    `,
      [user.firstName, user.lastName, user.username]
    );
    user.id = result.insertId;
    return;
  }

  return await db.execute(
    `update users
    set
      first_name = ?,
      last_name = ?,
      username = ?
    where id = ?`,
    [user.firstName, user.lastName, user.username, user.id]
  );
}

//active record ...????
async function remove(db, user) {
  return await db.execute(`delete from users where id = ?`, [user.id]);
}

//repo
async function removeById(db, id) {
  return await db.execute(`delete from users where id = ?`, [id]);
}

module.exports = {
  find,
  findAll,
  findByUsername,
  store,
  remove
};
