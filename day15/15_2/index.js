const mysql2 = require("mysql2/promise");
const User = require("./repository/user");

async function main() {
  const db = await mysql2.createConnection({
    host: "localhost",
    user: "root",
    database: "hw15"
  });

  // const user1 = await User.find(db, 1);
  // user1.firstName = "tester_repo";
  // await User.store(db, user1);

  //search and remove
  // const user1 = await User.find(db, 3);
  // await User.remove(db, user1);

  //search and edit name
  // const user1 = await User.findByUsername(db, "u7");
  // user1.firstName = "name_777777777777";
  // await User.store(db, user1);

  //remove
  // await User.removeByid(db, 5);
}

main();
