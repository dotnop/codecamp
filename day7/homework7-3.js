let input1 = [1, 2, 3];
let input2 = { a: 1, b: 2 };
let input3 = [1, 2, { a: 1, b: 2 }];
let input4 = [1, 2, { a: 1, b: { c: 3, d: 4 } }];

function getType(something) {
  return something.constructor.name;
}

function cloneObj(Thing) {
  let type = getType(Thing);
  if (type == "Array") {
    let newObject = Thing.map(function(value) {
      return cloneObj(value);
    });
    return newObject;
  } else if (type == "Object") {
    let newObject = {};
    for (let key in Thing) {
      newObject[key] = cloneObj(Thing[key]);
    }
    return newObject;
  } else {
    return Thing;
  }
}

exports.cloneObj = cloneObj;
