const assert = require("assert");
let hw = require("./homework7-3");
let input1 = [1, 2, 3];
let input2 = { a: 1, b: 2 };
let input3 = [1, 2, { a: 1, b: 2 }];
let input4 = [1, 2, { a: 1, b: { c: 3, d: 4 } }];

describe("Clone function give value fron original", function() {
  it("should accept [1, 2, 3]", function() {
    assert.deepEqual(hw.cloneObj(input1), input1);
  });
  it("should accept { a: 1, b: 2 }", function() {
    assert.deepEqual(hw.cloneObj(input2), input2);
  });
  it("should accept [1, 2, { a: 1, b: 2 }]", function() {
    assert.deepEqual(hw.cloneObj(input3), input3);
  });
  it("should accept [1, 2, { a: 1, b: { c: 3, d: 4 } }]", function() {
    assert.deepEqual(hw.cloneObj(input4), input4);
  });
});
describe("Test edit the first clone value after clone finish", function() {
  it("Clone should change and not equal [1, 2, 3]", function() {
    let newObj = hw.cloneObj(input1);
    newObj[0] = 0;
    assert.notDeepEqual(newObj, input1);
  });
  it("Clone should change and not equal { a: 1, b: 2 }", function() {
    let newObj = hw.cloneObj(input2);
    newObj[0] = 0;
    assert.notDeepEqual(newObj, input2);
  });
  it("Clone should change and not equal [1, 2, { a: 1, b: 2 }]", function() {
    let newObj = hw.cloneObj(input3);
    newObj[0] = 0;
    assert.notDeepEqual(newObj, input3);
  });
  it("Clone should change and not equal [1, 2, { a: 1, b: { c: 3, d: 4 } }]", function() {
    let newObj = hw.cloneObj(input4);
    newObj[0] = 0;
    assert.notDeepEqual(newObj, input4);
  });
});
describe("Proved the clone value has change to new thing", function() {
  it("Clone should change to [ 0, 2, 3 ]", function() {
    let newObj = hw.cloneObj(input1);
    newObj[0] = 0;
    assert.deepEqual(newObj, [0, 2, 3]);
  });
  it("Clone should change to { '0': 0, a: 1, b: 2 }", function() {
    let newObj = hw.cloneObj(input2);
    newObj[0] = 0;
    assert.deepEqual(newObj, { "0": 0, a: 1, b: 2 });
  });
  it("Clone should change to [ 0, 2, { a: 1, b: 2 } ]", function() {
    let newObj = hw.cloneObj(input3);
    newObj[0] = 0;
    assert.deepEqual(newObj, [0, 2, { a: 1, b: 2 }]);
  });
  it("Clone should change to  [ 0, 2, { a: 1, b: { c: 3, d: 4 } } ]", function() {
    let newObj = hw.cloneObj(input4);
    newObj[0] = 0;
    assert.deepEqual(newObj, [0, 2, { a: 1, b: { c: 3, d: 4 } }]);
  });
});
describe("Test edit the last clone value after clone finish", function() {
  it("Clone should change and not equal [1, 2, 3]", function() {
    let newObj = hw.cloneObj(input1);
    newObj[2] = 0;
    assert.notDeepEqual(newObj, input1);
  });
  it("Clone should change and not equal { a: 1, b: 2 }", function() {
    let newObj = hw.cloneObj(input2);
    newObj.b = 0;
    assert.notDeepEqual(newObj, input2);
  });
  it("Clone should change and not equal [1, 2, { a: 1, b: 2 }]", function() {
    let newObj = hw.cloneObj(input3);
    newObj[2].b = 0;
    assert.notDeepEqual(newObj, input3);
  });
  it("Clone should change and not equal [1, 2, { a: 1, b: { c: 3, d: 4 } }]", function() {
    let newObj = hw.cloneObj(input4);
    newObj[2].b.d = 0;
    assert.notDeepEqual(newObj, input4);
  });
});
describe("Proved the clone value has change to new thing", function() {
  it("Clone should change to [ 0, 2, 3 ]", function() {
    let newObj = hw.cloneObj(input1);
    newObj[0] = 0;
    assert.deepEqual(newObj, [0, 2, 3]);
  });
  it("Clone should change to { '0': 0, a: 1, b: 2 }", function() {
    let newObj = hw.cloneObj(input2);
    newObj[0] = 0;
    assert.deepEqual(newObj, { "0": 0, a: 1, b: 2 });
  });
  it("Clone should change to [ 0, 2, { a: 1, b: 2 } ]", function() {
    let newObj = hw.cloneObj(input3);
    newObj[0] = 0;
    assert.deepEqual(newObj, [0, 2, { a: 1, b: 2 }]);
  });
  it("Clone should change to  [ 0, 2, { a: 1, b: { c: 3, d: 4 } } ]", function() {
    let newObj = hw.cloneObj(input4);
    newObj[0] = 0;
    assert.deepEqual(newObj, [0, 2, { a: 1, b: { c: 3, d: 4 } }]);
  });
});
