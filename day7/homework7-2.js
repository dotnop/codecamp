const fs = require("fs");

function readfile() {
  return new Promise(function(resolve, reject) {
    fs.readFile("./homework1.json", "utf8", function(err, data) {
      let info = JSON.parse(data);
      if (err) reject(err);
      else {
        resolve(info);
      }
    });
  });
}

function addYearSalary(row) {
  row.yearSalary = parseInt(row["salary"]) * 12;
  return row;
}

function addNextSalary(row) {
  let nextSalary = [];
  nextSalary[0] = parseInt(row["salary"]) * 1.1;
  nextSalary[1] = parseInt(row["salary"]) * 1.21;
  nextSalary[2] = parseInt(row["salary"]) * 1.331;
  row.nextSalary = nextSalary;
  return row;
}

function cloneObj(object) {
  let returnObj = {};
  for (let key in object) {
    returnObj[key] = object[key];
  }
  return returnObj;
}

function addAdditionalFields(employees) {
  let result = employees.map(function(row) {
    addYearSalary(row);
    addNextSalary(row);
    let newRow = cloneObj(row);
    return newRow;
  });
  return result;
}

async function readInfo() {
  try {
    let employees = await readfile();
    let newEmployees = await addAdditionalFields(employees);
    //let person1 = addYearSalary(employees[0]);
    //let person1 = addNextSalary(employees[0]);
    console.log(newEmployees);
    console.log("---------------------------------------------------------");
    newEmployees[0].salary = 0;
    console.log(newEmployees[0]);
    console.log("---------------------------------------------------------");
    console.log(employees[0]);
  } catch (error) {
    console.error(error);
  }
}

readInfo();
