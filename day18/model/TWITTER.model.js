const multer = require("koa-multer");
const ufile = multer({ dest: "upload/" });
const jimp = require("jimp");
const fs = require("fs");

async function upload(ctx) {
  await ufile.single("file")(ctx);
  let tempFile = ctx.req.file.path;
  let outFile = await jimp.read(tempFile);

  outFile
    .resize(100, 100)
    .quality(60)
    .write(tempFile + ".jpg");

  fs.unlink(tempFile, () => {});
  return ctx.req.file.path + ".jpg";
}

module.exports = {
  upload
};
