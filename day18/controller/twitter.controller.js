const twitter = require("../model/TWITTER.model");

function index(ctx) {
  ctx.body = "use post man!";
}

async function upload(ctx) {
  ctx.body = await twitter.upload(ctx);
}

module.exports = {
  index,
  upload
};
