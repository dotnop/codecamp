const Koa = require("koa");
const Router = require("koa-router");
const bodyParser = require("koa-bodyparser");

const Twitter = require("./controller/twitter.controller");

let appPort = 4000;

const router = new Router()
  .get("/", Twitter.index)
  .post("/upload", Twitter.upload);

const app = new Koa()
  .use(bodyParser())
  .use(router.routes())
  .listen(appPort, () => {
    console.log("server listen on port " + appPort);
  });
