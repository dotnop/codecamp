const user = require('../repo/user');
const multer = require('koa-multer');
const ufile = multer({ dest: 'upload/' });
const bcrypt = require('bcrypt');
const jimp = require('jimp');
const fs = require('fs');

module.exports = function(db) {
  return {
    async signup(ctx, db) {
      let data = ctx.request.body;
      let user_id = 0;
      let result = {};
      if (data) {
        data.password = await bcrypt.hashSync(data.password, 10);
        user_id = await user.signup(ctx.db, data);
        // console.log(data);
      } else {
        ctx.status = 404;
        ctx.body = 'not found';
      }
      if (user_id) {
        ctx.body = result;
      }
    },

    async signin(ctx) {
      await ctx.render('index');
    },

    async signout(ctx) {},

    async verify(ctx) {},

    async update(ctx) {},

    async follow(ctx) {},

    async unfollow(ctx) {},

    async listFollow(ctx) {},

    async listFollowed(ctx) {},

    async uploadPhoto(ctx) {
      await ufile.single('file')(ctx);
      let tempFile = ctx.req.file.path;
      let rawPhoto = await jimp.read(tempFile);

      let saveFileName = tempFile;

      ctx.body = await user.resizePhoto(rawPhoto, saveFileName);
      fs.unlink(tempFile, () => {});
    },
  };
};
