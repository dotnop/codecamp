module.exports = { requestLogger };

async function requestLogger(ctx, next) {
  const start = process.hrtime();
  await next();
  const diff = process.hrtime(start);
  console.log(`${ctx.method} ${ctx.path} ${(diff[0] * 1e9 + diff[1]) / 1e6}ms`);
}
