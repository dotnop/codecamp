module.exports = {
  async signin(db, user) {
    console.log(db);
    const result = await db.execute(
      `SELECT * FROM users WHERE username = ? AND password = ?`,
      [user.username, user.password]
    );
    return result[0];
  },

  async signup(db, user) {
    console.log(db);
    const result = await db.execute(
      `insert into users (
          username, email, password, name, status
        ) values (?, ?, ?, ?, ?)`,
      [user.username, user.email, user.password, user.name, 0]
    );
    return result[0].insertId;
  },

  async changePhoto(db, userId, photoUrl) {
    await db.execute(
      `update users set
          photo = ?
        where id = ?`,
      [photoUrl, userId]
    );
  },

  async resizePhoto(rawfile, saveFileName) {
    let photoUrl = saveFileName + ".jpg";
    await rawfile
      .resize(100, 100)
      .quality(60)
      .write(photoUrl);
    return photoUrl;
  },

  async changeCover(db, userId, photoUrl) {
    await db.execute(
      `
        update users set
          cover = ?
        where id = ?
      `,
      [photoUrl, userId]
    );
  },

  async follow(db, followerId, followingId) {
    await db.execute(
      `
        insert into follows (
          follower_id, following_id
        ) values (
          ?, ?
        )
      `,
      [followerId, followingId]
    );
  },

  async unfollow(db, followerId, followingId) {
    await db.execute(
      `
        delete from follows
        where follower_id = ? and following_id = ?
      `,
      [followerId, followingId]
    );
  },

  async changePassword(db, userId, oldPassword, newPassword) {
    oldPassword;
    let [result] = await db.execute(
      `
        select from follows
        where follower_id = ? and following_id = ?
      `,
      [followerId, followingId]
    );
  }
};
