'use strict';

const Koa = require('koa');
const Router = require('koa-router');
const session = require('koa-session');
const render = require('koa-ejs');
const path = require('path');
const app = new Koa();
const bodyParser = require("koa-bodyparser")
const pool = require('./lib/db');
const db = pool.getConnection();
const User = require('./controller/user.controller')(db);
//const Tweet = require("./controller/tweet.controller")(db);
// const Mes = require("./controller/mess.controller")(db)
// const Noti = require("./controller/noti.controller")(db)
const logger = require('./repo/utility');
const appPort = 4000;

render(app, {
  root: path.join(__dirname, 'view'),
  layout: 'template',
  viewExt: 'ejs',
  cache: false,
  debug: true,
});

const router = new Router()
  .get('/', User.signin)
  .del("/tweet/:id/like", Tweet.unlike)
  .del("/user/:id/follow", User.unfollow)
  .get("/message", Mes.getall)
  .get("/message/:userId", Mes.get)
  .get("/notification", Noti.allnoi)
  .get("/tweet", Tweet.getall)
  .get("/user/:id/follow", User.listFollow)
  .get("/user/:id/followed", User.listFollowed)
  .post("/auth/signin", User.signin)
  .post("/auth/signout", User.signout)
  .post("/auth/signup", User.signup)
  .post("/auth/verify", User.verify)
  .post("/message/:userId", Mes.create)
  .post("/tweet", Tweet.create)
  .post("/tweet/:id/reply", Tweet.reply)
  .post("/tweet/:id/retweet", Tweet.retweet)
  .post("/upload", User.uploadPhoto)
  .put("/tweet/:id/like", Tweet.dolike)
  .put("/tweet/:id/vote/:voteId", Tweet.vote)
  .put("/user/:id/follow", User.follow)
  .patch("/user/:id", User.update)
  // .get('/', async ctx => {
  //   let n = ctx.session.views || 0;
  //   let n2;

  //   if (ctx.session.myStupidVariable && ctx.session.myStupidVariable.length > 0)
  //     n2 = ctx.session.myStupidVariable.length;
  //   else {
  //     n2 = 0;
  //     ctx.session.myStupidVariable = [];
  //   }
  //   ctx.session.views = ++n;
  //   ctx.session.myStupidVariable.push(++n2);
  //   ctx.body =
  //     n +
  //     ' views | n2 ' +
  //     n2 +
  //     ' views | ' +
  //     JSON.stringify(ctx.session.myStupidVariable);
  // })
  .get('/test_view', async ctx => {
    await ctx.render('index');
  })
  .post('/check_login', async ctx => {
    console.log(ctx.request.body.username);
    await ctx.render('check_login', {
      username: ctx.request.body.username,
    });
  });

const sessionStore = {};

const CONFIG = {
  key: 'koa:sess' /** (string) cookie key (default is koa:sess) */,
  /** (number || 'session') maxAge in ms (default is 1 days) */
  /** 'session' will result in a cookie that expires when session/browser is closed */
  /** Warning: If a session cookie is stolen, this cookie will never expire */
  maxAge: 60 * 60 * 1000,
  overwrite: true /** (boolean) can overwrite or not (default true) */,
  httpOnly: true /** (boolean) httpOnly or not (default true) */,
  store: {
    get(key, maxAge, { rolling }) {
      return sessionStore[key];
    },
    set(key, sess, maxAge, { rolling, changed }) {
      sessionStore[key] = sess;
    },
    destroy(key) { },
  },
};

app.keys = ['asdf;lkdsa naksldflkdsajflkdsa'];
app
  .use(bodyParser())
  .use(session(CONFIG, app))
  // .use(logger.requestLogger)
  .use(router.routes())
  .listen(appPort, () => {
    console.log('server listen on port ' + appPort);
  });
