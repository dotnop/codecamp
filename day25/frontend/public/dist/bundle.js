/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "dist";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _lib = __webpack_require__(1);

function addProduct() {
  var div = document.getElementById("root");
  var pid = document.getElementById("pid").innerText;
  var title = document.getElementById("ptitle").innerText;
  var price_text = document.getElementById("pprice").innerText;
  var date_text = document.getElementById("pdate").innerText;
  var image_url = document.getElementById("purl").innerText;
  if (pid) {
    if (!title) {
      title = "Untitled";
    }
    if (!price_text) {
      price_text = " - ";
    }
    if (!image_url) {
      title = "./images/noimage.gif";
    }
  } else {
    alert("invalid ID");
  }
  //   let html = `<div class="pitem col-lg-3  col-md-4 col-sm-6" id="pid-${pid}">
  //           <div class="pname">${title}</div>
  //           <img src="${image_url}" alt="${title}">
  //           <div class="pprice">price ${price_text} ฿
  //               <button class="btn btn-success ">Add to cart
  //                   <i class="glyphicon glyphicon-shopping-cart pull-right "></i>
  //               </button>
  //           </div>
  //       </div>`;
  var html = "123";
  div.innerText = html;
  console.log(html);
}

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _domUtil = __webpack_require__(2);

Object.defineProperty(exports, "domUtil", {
  enumerable: true,
  get: function get() {
    return _domUtil.domUtil;
  }
});

var _bomUtil = __webpack_require__(3);

Object.defineProperty(exports, "bomUtil", {
  enumerable: true,
  get: function get() {
    return _bomUtil.bomUtil;
  }
});

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var domUtil = exports.domUtil = function () {
  function domUtil() {
    _classCallCheck(this, domUtil);
  }

  _createClass(domUtil, null, [{
    key: "setText",
    value: function setText(id, text) {
      var elem = document.getElementById(id);

      elem.innerText = text;
    }
  }, {
    key: "setHTML",
    value: function setHTML(id, html) {
      var elem = document.getElementById(id);

      elem.innerHTML = html;
    }
  }, {
    key: "addItems",
    value: function addItems(id, data) {
      var parent = document.getElementById(id);

      var ul = document.createElement("ul");

      ul.id = "menu";

      data.forEach(function (a) {
        console.log("a", a);

        var li = document.createElement("li");

        //example 4-5
        // li.id = a.id
        // li.style.color = 'blue'
        // li.style.cursor = 'pointer'
        //example 4-6
        // li.addEventListener("click", () => alert(a.text))

        li.appendChild(document.createTextNode(a.text));

        ul.appendChild(li);
      });

      parent.appendChild(ul);
    }
  }, {
    key: "editItem",
    value: function editItem(id, text) {
      var elem = document.getElementById(id);

      elem.innerText = text;
    }
  }, {
    key: "removeItem",
    value: function removeItem(removeId, parentId) {
      var elem = document.getElementById(removeId);
      var parent = document.getElementById(parentId);
      parent.removeChild(elem);
    }
  }]);

  return domUtil;
}();

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var bomUtil = exports.bomUtil = function () {
  function bomUtil() {
    _classCallCheck(this, bomUtil);
  }

  _createClass(bomUtil, null, [{
    key: "openWindow",
    value: function openWindow() {
      this.win.window.open("https://www.w3schools.com", "", "with=200,height=100");
    }
  }, {
    key: "closeWindow",
    value: function closeWindow() {
      if (this.win) {
        this.win.close();
      }
    }
  }]);

  return bomUtil;
}();

/***/ })
/******/ ]);