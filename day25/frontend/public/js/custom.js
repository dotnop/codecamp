var Books = {};

function getDataFromServer() {
  let url = "http://localhost:3000/books";
  let data = {};
  fetch(url, {
    method: "GET",
    body: data,
    headers: new Headers({
      "Content-Type": "application/json"
    })
  })
    .then(res => res.json())
    .then(data => {
      Books = data;
      showBooks();
    })
    .catch(error => console.error("Error:", error))
    .then(response => console.log("Success:", response));
}

// function addDataToServer(data) {
//   let url = "http://localhost:3000/books";
//   console.log("JSON Book data", data);
//   fetch(url, {
//     method: "POST",
//     mode: "no-cors",
//     body: JSON.stringify(data),
//     headers: {
//       Accept: "application/json",
//       "Content-Type": "application/json"
//     }
//   })
//     .then(res =>
//       res.json().then(data => {
//         showBook(data);
//       })
//     )
//     .catch(error => console.error("Error:", error))
//     .then(response => console.log("Success:", response));
// }
function addDataToServer(data) {
  console.log("new book:", data);
  fetch("http://localhost:3000/books", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify(data)
  })
    .then(res => {
      res.json().then(data => {
        //todo reduce loding time
        //showBook(data);
        getDataFromServer();
      });
    })
    .catch(error => console.error("Error:", error))
    .then(response => console.log("Success:", response));
}

function editDatOnServer(data) {
  console.log("edit book:", data.id);
  fetch("http://localhost:3000/books/" + data.id, {
    method: "PATCH",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify(data)
  })
    .then(res => {
      res.json().then(data => {
        //todo reduce loding time
        //showBook(data);
        getDataFromServer();
      });
    })
    .catch(error => console.error("Error:", error))
    .then(response => console.log("Success:", response));
}

function deleteDataFromServer(bookid) {
  console.log("remove book(id): ", bookid);
  fetch("http://localhost:3000/books/" + bookid, {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json"
    },
    body: {}
  })
    .then(res => {
      res.json().then(data => {
        //todo reduce loding time
        //$("#p" + bookid).remove();
        getDataFromServer();
      });
    })
    .catch(error => console.error("Error:", error))
    .then(response => console.log("Success:", response));
}

// function deleteDataFromServer(bookId) {
//   let url = "http://localhost:3000/books/" + bookId;
//   let data = {};
//   fetch(url, {
//     method: "DELETE",
//     mode: "no-cors",
//     body: data,
//     headers: new Headers({
//       "Content-Type": "application/json"
//     })
//   })
//     .then(res => res.json())
//     .then(data => {
//       $("#p" + bookid).remove();
//       alert("Book deleted");
//     })
//     .catch(error => console.error("Error:", error))
//     .then(response => console.log("Success:", response));
// }

function showBooks() {
  // Todo remove this for performance
  emptyContainer();
  Books.forEach(book => {
    let _pid = book["id"];
    let _pisbn = book["isbn"];
    let _ptitle = book["title"];
    let _purl = book["imageurl"];
    let _pprice = book["price"];
    let _pdate = "";

    let markup = `
    <div class="pitem col-lg-3  col-md-4" id="p${_pid}">
      <div class="pisbn">${_pisbn}</div>
      <div class="ptitle">${_ptitle}</div>
      <div class="pdate hidden">${_pdate}</div>
        <img src="${_purl}" alt="${_ptitle}">
        <div> price <span class="pprice">${_pprice}</span> ฿</div>
        <div class="btn btn-success ">Add to cart
         <i class="glyphicon glyphicon-shopping-cart "></i>
        </div>
        <div class="btn-group">  
          <div class="btn btn-success edit" onclick="modaleditProduct('${_pid}')" data-id="${_pid}">edit
          <i class="glyphicon glyphicon-edit"></i>
       </div>
       <div class="btn btn-success delete"  onclick="deleteProduct('${_pid}')" data-id="${_pid}">delete
         <i class="glyphicon glyphicon-remove"></i>
        </div>
        </div>
      </div>
    </div>`;
    $("#newproduct").prepend(markup);
  });
}

function emptyContainer() {
  $("#newproduct").empty();
}

function showBook(book) {
  let _pid = book["id"];
  let _pisbn = book["isbn"];
  let _ptitle = book["title"];
  let _purl = book["imageurl"];
  let _pprice = book["price"];
  let _pdate = "";

  let markup = `
    <div class="pitem col-lg-3  col-md-4" id="p${_pid}">
      <div class="pisbn">${_pisbn}</div>
      <div class="ptitle">${_ptitle}</div>
      <div class="pdate hidden">${_pdate}</div>
        <img src="${_purl}" alt="${_ptitle}">
        <div> price <span class="pprice">${_pprice}</span> ฿</div>
        <div class="btn btn-success ">Add to cart
         <i class="glyphicon glyphicon-shopping-cart "></i>
        </div>
        <div class="btn-group">  
          <div class="btn btn-success edit" onclick="modaleditProduct('${_pid}')" data-id="${_pid}">edit
          <i class="glyphicon glyphicon-edit"></i>
       </div>
       <div class="btn btn-success delete"  onclick="deleteProduct('${_pid}')" data-id="${_pid}">delete
         <i class="glyphicon glyphicon-remove"></i>
        </div>
        </div>
      </div>
    </div>`;
  $("#newproduct").prepend(markup);
}

function addProduct() {
  let obj = {
    id: "",
    isbn: "",
    title: "",
    imageurl: "",
    price: "",
    date: ""
  };
  obj.id = "";
  obj.isbn = $("#aisbn").val();
  obj.title = $("#atitle").val();
  obj.imageurl = $("#aurl").val();
  obj.price = $("#aprice").val();
  obj.date = "";
  addDataToServer(obj);
  //save to JSON.DB
  $("#addModal").modal("hide");
}

function modaleditProduct(bookid) {
  Books.forEach(book => {
    if (book.id == bookid) {
      let pid = book.id;
      let pisbn = book.isbn;
      let ptitle = book.title;
      let purl = book.imageurl;
      let pprice = book.price;
      let pdate = "";
      $("#eisbn").val(pisbn);
      $("#etitle").val(ptitle);
      $("#eurl").val(purl);
      $("#eprice").val(pprice);
      $("#edate").val(pdate);
      $("#econtainer").html(
        `<div class="btn btn-success" id="btnsubmitedit" onclick="saveEditProduct('${bookid}')">Submit Edit</div>`
      );
      $("#editModal").modal("show");
    }
  });
}

function saveEditProduct(bookid) {
  //change data in client
  let sid = "#p" + bookid;
  let sisbn = $("#eid").val();
  let stitle = $("#etitle").val();
  let surl = $("#eurl").val();
  let sprice = $("#eprice").val();
  let sdate = $("#edate").val();

  $(bookid + " .pisbn").text(sisbn);
  $(bookid + " .ptitle").text(stitle);
  $(bookid + " .purl").attr("src", surl);
  $(bookid + " .pprice").text(sprice);
  $(bookid + " .pdate").text(sdate);

  //change data on server
  let obj = {
    id: "",
    isbn: "",
    title: "",
    imageurl: "",
    price: "",
    date: ""
  };
  obj.id = bookid;
  obj.isbn = sisbn;
  obj.title = stitle;
  obj.imageurl = surl;
  obj.price = sprice;
  obj.date = sdate;
  editDatOnServer(obj);
  //save to JSON.DB
  $("#editModal").modal("hide");
}

function deleteProduct(bookid) {
  if (confirm("Delete this?")) {
    deleteDataFromServer(bookid);
  }
}
