import React, { Component } from 'react';
import { Layout, Form, Input, Icon, Row, Col, Button, Card, List, Avatar } from 'antd';
import logo from './logo.svg';
import './App.css';



class App extends Component {

  constructor(props) {
    super(props);

    this._class = '';
    this.state = {
      inputText: '',
      listItem: []
    }

    this.handleChangeText = this.handleChangeText.bind(this);

  }

  deleteListAtIndex = (index) => {
    // ไม่ควรทำเพราะเป็นการ Render ใหม่ทั้ง State ถ้ามีเยอะก็ฉิบหายยย สิครับ
    // this.state.listItem.splice(index, 1);
    // this.setState({});

    const result = this.state.listItem;
    result.splice(index, 1);
    this.setState({ listItem: result });
  }

  submitList = () => {
    if (this.state.inputText !== '') {
      this.setState({
        listItem: this.state.listItem.concat([this.state.inputText]),
        inputText: ''
      })
    }
  }

  handleKeyPress = (event) => {
    if (event.key === 'Enter') {
      this.submitList();
    }
  }

  handleChangeText = (event) => {
    this.setState({ inputText: event.target.value });
  }
  render() {

    // const data = [
    //     'text 1',
    //     'text 2',
    //     'text 3',
    // ];

    const { Header, Footer, Sider, Content } = Layout;
    const Search = Input.Search;
    const FormItem = Form.Item;

    return (
      <div>
        <Card style={{ width: '100%', height: '100%', backgroundColor: this.props.myColor }}>
          <List
            bordered
            dataSource={this.state.listItem}
            renderItem={(item, index) => (
              index % 2 == 0 ?
                <List.Item>
                  <div className="aleft"><Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />{item}</div>
                </List.Item>
                :
                <List.Item>
                  <div className="aright">{item}<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" /></div>
                </List.Item>
            )}
          />
          <div className="chatbox">
            <Input
              addonAfter={<Button type="primary" onClick={this.submitList}>Add</Button>}
              onChange={this.handleChangeText}
              value={this.state.inputText}
              onKeyPress={this.handleKeyPress} />
          </div>
        </Card>
      </div>

    );
  }
}

export default App;
