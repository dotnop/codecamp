function addProduct() {
  let _pid = $("#aid").val();
  let _ptitle = $("#atitle").val();
  let _purl = $("#aurl").val();
  let _pprice = $("#aprice").val();
  let _pdate = $("#adate").val();
  let markup = `
  <div class="pitem col-lg-3  col-md-4" id="p${_pid}">
    <div class="ptitle">${_ptitle}</div>
    <div class="pdate hidden">${_pdate}</div>
      <img src="${_purl}" alt="${_ptitle}">
      <div> price <span class="pprice">${_pprice}</span> ฿</div>
      <div class="btn btn-success ">Add to cart
       <i class="glyphicon glyphicon-shopping-cart pull-right " />
      </div>
      <div class="btn-group">  
        <div class="btn btn-success edit" data-id="p${_pid} ">edit
        <i class="glyphicon glyphicon-edit pull-right" />
     </div>
     <div class="btn btn-success delete" data-id="p  ${_pid} ">delete
       <i class="glyphicon glyphicon-remove pull-right" />
      </div>
      </div>
    </div>
  </div>`;
  $("#newproduct").prepend(markup);
}

function modaleditProduct(bookid) {
  let ptitle = $("#" + bookid + " .ptitle").text();
  let purl = $("#" + bookid + " .purl").attr("src");
  let pprice = $("#" + bookid + " .pprice").text();
  let pdate = $("#" + bookid + " .pdate").text();
  $("#eid").val(bookid);
  $("#etitle").val(ptitle);
  $("#eurl").val(purl);
  $("#eprice").val(pprice);
  $("#edate").val(pdate);
  $("#editModal").modal("show");
}

function saveEditProduct() {
  let bookid = "#" + $("#eid").val();
  let stitle = $("#etitle").val();
  let surl = $("#eurl").val();
  let sprice = $("#eprice").val();
  let sdate = $("#edate").val();

  $(bookid + " .ptitle").text(stitle);
  $(bookid + " .purl").attr("src", surl);
  $(bookid + " .pprice").text(sprice);
  $(bookid + " .pdate").text(sdate);
  $("#editModal").modal("hide");
}

function deleteProduct(bookid) {
  if (confirm("Delete this?")) {
    $("#" + bookid).remove();
  }
}
