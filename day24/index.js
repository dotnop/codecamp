const Koa = require("koa");
const serve = require("koa-static");
const app = new Koa();
const formidable = require("koa2-formidable");

app.use(serve("public")).use(formidable());

const PORT = 3000;

app.listen(PORT, () => {
  console.log(`Service is running on port ${PORT}.`);
});
