export class domUtil {
  constructor() {}

  static setText(id, text) {
    let elem = document.getElementById(id);

    elem.innerText = text;
  }

  static setHTML(id, html) {
    let elem = document.getElementById(id);

    elem.innerHTML = html;
  }

  static addItems(id, data) {
    let parent = document.getElementById(id);

    let ul = document.createElement("ul");

    ul.id = "menu";

    data.forEach(a => {
      console.log("a", a);

      let li = document.createElement("li");

      //example 4-5
      // li.id = a.id
      // li.style.color = 'blue'
      // li.style.cursor = 'pointer'
      //example 4-6
      // li.addEventListener("click", () => alert(a.text))

      li.appendChild(document.createTextNode(a.text));

      ul.appendChild(li);
    });

    parent.appendChild(ul);
  }

  static editItem(id, text) {
    let elem = document.getElementById(id);

    elem.innerText = text;
  }

  static removeItem(removeId, parentId) {
    let elem = document.getElementById(removeId);
    let parent = document.getElementById(parentId);
    parent.removeChild(elem);
  }
}
