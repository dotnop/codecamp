import $ from 'jquery'
import { fetchApi } from './middlewares'

// example 5-1
$('#btnShow').click(() => $('p').show())
$('#btnHide').click(() => $('p').hide())

// example 5-2
$('#btnFadeIn').click(() => $('#fadeContent').fadeIn('slow'))
$('#btnFadeOut').click(() => $('#fadeContent').fadeOut(1000))

// example 5-3
$('#btnSlideDown').click(() => $('#slideContent').slideDown('slow'))
$('#btnSlideUp').click(() => $('#slideContent').slideUp(1000))
$('#btnSlideToggle').click(() => $('#slideContent').slideToggle(1000))

// example 5-4
$('#btnAnimate').click(() => {
  $('#animate')
  .animate({height: '300px', opacity: '0.4'}, "slow")
  .animate({width: '300px', opacity: '0.8'}, "slow")
  .animate({height: '100px', opacity: '0.4'}, "slow")
  .animate({width: '100px', opacity: '0.8'}, "slow")
})

// example 5-5
$('#btnGet').click(() => {
  console.log($('#pGet').text())
  console.log($('#pGet').html())
  console.log($('#txtInput').val())
}) 

let api = new fetchApi()
api.get('http://localhost:3000/books')
.then(res => {
  res.json().then(result => console.log(result))
})
