import React from 'react'
import { Switch, Route } from 'react-router-dom'
import  Todo  from './Todo';
import  Todo2  from './Todo2';
import  Todo3  from './Todo3';

// The Main component renders one of the three provided
// Routes (provided that one matches). Both the /roster
// and /schedule routes will match any pathname that starts
// with /roster or /schedule. The / route will only match
// when the pathname is exactly the string "/"
const Main = () => (
  <main>
    <Switch>
      <Route exact path='/' component={Todo}/>
      <Route exact path='/todo2' component={Todo2}/>
      <Route exact path='/todo2' component={Todo3}/>
    </Switch>
  </main>
)

export default Main
