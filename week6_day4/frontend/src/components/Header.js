import React from 'react'
import { Link } from 'react-router-dom'

// The Header creates links that can be used to navigate
// between routes.
const Header = () => (
  <header>
    <nav>
      <ul>
        <li><Link to='/'>Todo MyAPI</Link></li>
        <li><Link to='/Todo2'>Todo MockAPI</Link></li>
        <li><Link to='/Todo3'>Todo Firebase</Link></li>
      </ul>
    </nav>
  </header>
)

export default Header
