import React, { Component } from 'react';

import { Input, Icon, Button, Card, List, Spin } from 'antd';

export class Todo extends Component {

  constructor(props) {
    super(props);

    this.state = {
      inputText: '',
      listItem: [],
      isLoading: true
    }

    this.handleChangeText = this.handleChangeText.bind(this);

  }

  componentDidMount() {
    // เราควรจะ fetch เพื่อเอาค่ามาจาก MockAPI 
    this.fetchGet();
  }

  async fetchGet() {
    const result = await fetch("http://localhost:4000/todos")
    if (result.ok) {
      let data = await result.json();
      let listItem = [...data];
      this.setState({ listItem, isLoading: false });
    }
  }

  async fetchPost(text) {
    //this.setState({ isLoading: true });
    if (text) {
      const created_at = new Date();
      const modified_at = created_at;
      const completed_at = null;
      const pudh_data = JSON.stringify({
        "text": text,
        "created_at": created_at,
        "modified_at": modified_at,
        "completed_at": completed_at
      });
      console.log(pudh_data);
      const result = await fetch('http://localhost:4000/todos', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: pudh_data,
      })

      if (result.ok) {
        // ท่านี้ก็ได้ดูดีกว่า 1
        let data = await result.json();
        let listItem = [data, ...this.state.listItem];
        console.log(listItem)
        this.setState({ listItem, isLoading: false })

        // ท่านี้ก็ได้ดูดีกว่า 2
        //this.fetchGet();
      }

    }
  }

  // shouldComponentUpdate(state, props) {
  //   return true
  // }

  deleteListAtIndex = async (index, id) => {
    // ไม่ควรทำเพราะเป็นการ Render ใหม่ทั้ง State ถ้ามีเยอะก็ฉิบหายยย สิครับ
    // this.state.listItem.splice(index, 1);
    // this.setState({});
    this.setState({ isLoading: true });
    console.log('index:', index, 'id:', id);
    if (!isNaN(id)) {
      const res = await fetch('http://localhost:4000/todos/' + id, {
        method: 'DELETE'
      });
      if (res.ok) {
        // this.fetchGet();
        this.state.listItem.splice(index, 1);
        //where is list item??? magic update??
        //hint in pointer this.state.listItem.
        this.setState({ isLoading: false });
        console.log('Current state: ', this.state)
      }
      return
    }
  }

  submitList = () => {
    //console.log('submit list',this.state.inputText)
    this.fetchPost(this.state.inputText);
    this.setState({
      //listItem: this.state.listItem.concat([this.state.inputText]),
      inputText: ''
    })
    //console.log(this.state.listItem);
  }

  handleKeyPress = (event) => {
    if (event.key === 'Enter') {
      this.submitList();
    }
  }

  handleChangeText = (event) => {
    this.setState({ inputText: event.target.value });
  }

  render() {

    // const data = [
    //     'text 1',
    //     'text 2',
    //     'text 3',
    // ];

    //const { Header, Footer, Sider, Content } = Layout;
    //const Search = Input.Search;
    //const FormItem = Form.Item;

    // หลัง Return มันต้องมี DIV ครอบก่อน
    // { if 1==1 ? 'TRUE' : 'FALSE'}
    return (
      <div>
        {
          this.state.isLoading === false ? <Card style={{ width: 500, backgroundColor: this.props.myColor }}>
            <h1>To-do-list</h1>

            <div style={{ marginBottom: '10px' }}>
              <Input
                addonAfter={<Button type="primary" onClick={this.submitList}>Add</Button>}
                onChange={this.handleChangeText}
                value={this.state.inputText}
                onKeyPress={this.handleKeyPress} />
            </div>

            <List
              bordered
              dataSource={this.state.listItem}
              renderItem={(item, index) => (
                <List.Item actions={[<a onClick={() => this.deleteListAtIndex(index, item.id)}><Icon type="close-circle" style={{ fontSize: 16, color: 'rgb(255, 145, 0)' }} /></a>]}>
                  {item.text}
                </List.Item>
              )}
            />
          </Card> : <Spin />
        }

      </div>
    );
  }
}
