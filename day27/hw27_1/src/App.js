import React, { Component } from "react";
import { Button, Input } from "antd";
import logo from "./logo.svg";
import "./App.css";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      result: 0,
      operant: "+",
      num: 0
    };
  }

  getResult = () => {
    this.setState({
      result: this.state.result.concat([this.target.value]),
      inputText: ""
    });
  };

  handleClick = event => {
    console.log(event.target.value);
    this.getResult();
  };

  // handleChangeText = event => {
  //   this.setState({ inputText: event.target.value });
  // };

  render() {
    return (
      <div id="background">
        <Input id="result" value={this.state.result} />
        <div id="main">
          <div id="first-rows">
            <Button value="d" className="del-bg" id="delete" onClick={this.handleClick}> Del </Button>
            <Button value="%" className="btn-style operator opera-bg fall-back" onClick={this.handleClick}> % </Button>
            <Button value="+" className="btn-style opera-bg value align operator" onClick={this.handleClick}> + </Button>
          </div>
          <div class="rows">
            <Button value="7" className="btn-style num-bg num first-child" onClick={this.handleClick}> 7 </Button>
            <Button value="8" className="btn-style num-bg num" onClick={this.handleClick}> 8 </Button>
            <Button value="9" className="btn-style num-bg num" onClick={this.handleClick}> 9 </Button>
            <Button value="-" className="btn-style opera-bg operator" onClick={this.handleClick}> - </Button>
          </div>
          <div class="rows">
            <Button value="4" className="btn-style num-bg num first-child" onClick={this.handleClick}> 4 </Button>
            <Button value="5" className="btn-style num-bg num" onClick={this.handleClick}> 5 </Button>
            <Button value="6" className="btn-style num-bg num" onClick={this.handleClick}> 6 </Button>
            <Button value="*" className="btn-style opera-bg operator" onClick={this.handleClick}> x </Button>
          </div>
          <div class="rows">
            <Button value="1" className="btn-style num-bg num first-child" onClick={this.handleClick}> 1 </Button>
            <Button value="2" className="btn-style num-bg num" onClick={this.handleClick}> 2 </Button>
            <Button value="3" className="btn-style num-bg num" onClick={this.handleClick}> 3 </Button>
            <Button value="/" className="btn-style opera-bg operator" onClick={this.handleClick}> / </Button>
          </div>
          <div class="rows">
            <Button value="0" className="num-bg zero" id="delete" onClick={this.handleClick}> 0 </Button>
            <Button value="." className="btn-style num-bg period fall-back" onClick={this.handleClick}> . </Button>
            <Button value="=" id="eqn-bg" className="eqn align" onClick={this.handleClick}> Enter </Button>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
