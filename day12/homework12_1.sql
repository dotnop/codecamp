use lab12;

--  ม ใ ค ร บ า ง ท ไ ม ไ ด ส อ น อ ะ ไ ร เ ล ย
SELECT
    i.name AS instructor_name,
    c.name as courses_name
FROM
    instructors AS i
    LEFT JOIN courses AS c ON i.id = c.teach_by
WHERE
    c.id IS NULL
ORDER BY
    c.id;
-- +------------------+
-- | instructor_name  |
-- +------------------+
-- | Thomas Keller    |
-- | Martin Scorsese  |
-- | Alice Waters     |
-- | Bob Woofward     |
-- | Helen Mirren     |
-- | Ron Howard       |
-- | Armin Van Buuren |
-- +------------------+
-- 7 rows in set (0.00 sec)

-- ม ค อ ร ส ไ ห น บ า ง ท ไ ม ม ค น ส อ น
SELECT
    c.name AS course_name
FROM
    courses AS c
    LEFT JOIN instructors AS i ON i.id = c.teach_by
WHERE
    i.id IS NULL
ORDER BY
    i.id;
-- +-------------------------+
-- | course_name             |
-- +-------------------------+
-- | Database System Concept |
-- | JavaScript for Beginner |
-- | OWASP Top 10            |
-- +-------------------------+
-- 3 rows in set (0.00 sec)
