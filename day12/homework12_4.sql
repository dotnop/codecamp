use employees;

-- หาว่าพนักงานแต่ละคน มีใครเป็น manager บ้าง
SELECT
    e.emp_no AS employee_id,
    concat(e.first_name," ",e.last_name) AS employee_name,
    t.title AS title,
    se.employee_lastest_salary AS employee_salary,
    de.to_date AS salary_date,
    d.dept_name AS department_name,
    sm.emp_no AS manager_id,
    concat(em.first_name," ",em.last_name) AS manager_name,
    sm.manager_lastest_salary AS manager_salary,
    dm.to_date AS salary_date
FROM employees AS e
    INNER JOIN titles AS t ON t.emp_no = e.emp_no
    INNER JOIN dept_emp AS de ON de.emp_no = e.emp_no
    INNER JOIN
    (
        SELECT
            s.emp_no,
            s.salary AS employee_lastest_salary, 
            s.to_date AS employee_lastest_salary_date
        FROM salaries s
        GROUP BY s.emp_no
        ORDER BY `emp_no` DESC, `from_date` DESC 
    ) se ON  se.emp_no = e.emp_no AND de.to_date = se.employee_lastest_salary_date
    INNER JOIN departments AS d ON d.dept_no = de.dept_no
    INNER JOIN 
    (
        SELECT
            d.emp_no,
            d.to_date,
            d.dept_no
        FROM dept_manager d
        GROUP BY d.dept_no
        ORDER BY `dept_no` DESC, `from_date` DESC 
    ) dm ON dm.dept_no = de.dept_no
    INNER JOIN
    (
        SELECT
            s.emp_no AS emp_no,
            s.salary AS manager_lastest_salary,
            s.to_date AS manager_lastest_salary_date
        FROM salaries s
        GROUP BY s.emp_no
        ORDER BY `emp_no` DESC, `from_date` DESC
    ) sm ON  sm.emp_no = dm.emp_no AND dm.to_date = sm.manager_lastest_salary_date
    INNER JOIN employees AS em ON em.emp_no = dm.emp_no
GROUP BY e.emp_no
LIMIT 50;

-- +--------+------------+-------------+--------------+--------+------------+
-- | emp_no | birth_date | first_name  | last_name    | gender | hire_date  |
-- +--------+------------+-------------+--------------+--------+------------+
-- | 110022 | 1956-09-12 | Margareta   | Markovitch   | M      | 1985-01-01 |
-- | 110039 | 1963-06-21 | Vishwani    | Minakawa     | M      | 1986-04-12 |
-- | 110085 | 1959-10-28 | Ebru        | Alpin        | M      | 1985-01-01 |
-- | 110114 | 1957-03-28 | Isamu       | Legleitner   | F      | 1985-01-14 |
-- | 110183 | 1953-06-24 | Shirish     | Ossenbruggen | F      | 1985-01-01 |
-- | 110228 | 1958-12-02 | Karsten     | Sigstam      | F      | 1985-08-04 |
-- | 110303 | 1956-06-08 | Krassimir   | Wegerle      | F      | 1985-01-01 |
-- | 110344 | 1961-09-07 | Rosine      | Cools        | F      | 1985-11-22 |
-- | 110386 | 1953-10-04 | Shem        | Kieras       | M      | 1988-10-14 |
-- | 110420 | 1963-07-27 | Oscar       | Ghazalie     | M      | 1992-02-05 |
-- | 110511 | 1957-07-08 | DeForest    | Hagimont     | M      | 1985-01-01 |
-- | 110567 | 1964-04-25 | Leon        | DasSarma     | F      | 1986-10-21 |
-- | 110725 | 1961-03-14 | Peternela   | Onuegbe      | F      | 1985-01-01 |
-- | 110765 | 1954-05-22 | Rutger      | Hofmeyr      | F      | 1989-01-07 |
-- | 110800 | 1963-02-07 | Sanjoy      | Quadeer      | F      | 1986-08-12 |
-- | 110854 | 1960-08-19 | Dung        | Pesch        | M      | 1989-06-09 |
-- | 111035 | 1962-02-24 | Przemyslawa | Kaelbling    | M      | 1985-01-01 |
-- | 111133 | 1955-03-16 | Hauke       | Zhang        | M      | 1986-12-30 |
-- | 111400 | 1959-11-09 | Arie        | Staelin      | M      | 1985-01-01 |
-- | 111534 | 1952-06-27 | Hilary      | Kambil       | F      | 1988-01-31 |
-- | 111692 | 1954-10-05 | Tonny       | Butterworth  | F      | 1985-01-01 |
-- | 111784 | 1956-06-14 | Marjo       | Giarratana   | F      | 1988-02-12 |
-- | 111877 | 1962-10-18 | Xiaobin     | Spinelli     | F      | 1991-08-17 |
-- | 111939 | 1960-03-25 | Yuchang     | Weedman      | M      | 1989-07-10 |
-- +--------+------------+-------------+--------------+--------+------------+
-- 24 rows in set (0.15 sec)

-- หาว่าพนักงานคนไหน เงินเดือนมากกว่า manager บ้าง
-- add magic on 64,76 ... ask TA pls 
-- ORDER BY `emp_no` DESC, `from_date` DESC 

-- V.short
SELECT
    concat(e.first_name," ",e.last_name) AS employee_name,
    se.employee_lastest_salary AS employee_salary,
    concat(em.first_name," ",em.last_name) AS manager_name,
    sm.manager_lastest_salary AS manager_salary
FROM employees AS e
    INNER JOIN titles AS t ON t.emp_no = e.emp_no
    INNER JOIN dept_emp AS de ON de.emp_no = e.emp_no
    INNER JOIN
    (
        SELECT
            s.emp_no,
            s.salary AS employee_lastest_salary, 
            s.to_date AS employee_lastest_salary_date
        FROM salaries s
        GROUP BY s.emp_no
        ORDER BY `emp_no` DESC, `from_date` DESC 
    ) se ON  se.emp_no = e.emp_no AND de.to_date = se.employee_lastest_salary_date
    INNER JOIN departments AS d ON d.dept_no = de.dept_no
    INNER JOIN 
    (
        SELECT
            d.emp_no,
            d.to_date,
            d.dept_no
        FROM dept_manager d
        GROUP BY d.dept_no
        ORDER BY `dept_no` DESC, `from_date` DESC 
    ) dm ON dm.dept_no = de.dept_no
    INNER JOIN
    (
        SELECT
            s.emp_no AS emp_no,
            s.salary AS manager_lastest_salary,
            s.to_date AS manager_lastest_salary_date
        FROM salaries s
        GROUP BY s.emp_no
        ORDER BY `emp_no` DESC, `from_date` DESC
    ) sm ON  sm.emp_no = dm.emp_no AND dm.to_date = sm.manager_lastest_salary_date
    INNER JOIN employees AS em ON em.emp_no = dm.emp_no
WHERE se.employee_lastest_salary > sm.manager_lastest_salary AND
      de.to_date >= dm.to_date  
GROUP BY e.emp_no
LIMIT 50;

-- Long version
SELECT
    e.emp_no AS employee_id,
    concat(e.first_name," ",e.last_name) AS employee_name,
    t.title AS title,
    se.employee_lastest_salary AS employee_salary,
    de.to_date AS salary_date,
    d.dept_name AS department_name,
    sm.emp_no AS manager_id,
    concat(em.first_name," ",em.last_name) AS manager_name,
    sm.manager_lastest_salary AS manager_salary,
    dm.to_date AS salary_date
FROM employees AS e
    INNER JOIN titles AS t ON t.emp_no = e.emp_no
    INNER JOIN dept_emp AS de ON de.emp_no = e.emp_no
    INNER JOIN
    (
        SELECT
            s.emp_no,
            s.salary AS employee_lastest_salary, 
            s.to_date AS employee_lastest_salary_date
        FROM salaries s
        GROUP BY s.emp_no
        ORDER BY `emp_no` DESC, `from_date` DESC 
    ) se ON  se.emp_no = e.emp_no AND de.to_date = se.employee_lastest_salary_date
    INNER JOIN departments AS d ON d.dept_no = de.dept_no
    INNER JOIN 
    (
        SELECT
            d.emp_no,
            d.to_date,
            d.dept_no
        FROM dept_manager d
        GROUP BY d.dept_no
        ORDER BY `dept_no` DESC, `from_date` DESC 
    ) dm ON dm.dept_no = de.dept_no
    INNER JOIN
    (
        SELECT
            s.emp_no AS emp_no,
            s.salary AS manager_lastest_salary,
            s.to_date AS manager_lastest_salary_date
        FROM salaries s
        GROUP BY s.emp_no
        ORDER BY `emp_no` DESC, `from_date` DESC
    ) sm ON  sm.emp_no = dm.emp_no AND dm.to_date = sm.manager_lastest_salary_date
    INNER JOIN employees AS em ON em.emp_no = dm.emp_no
WHERE se.employee_lastest_salary > sm.manager_lastest_salary AND
      de.to_date >= dm.to_date  
GROUP BY e.emp_no
LIMIT 50;
