use lab12;

-- จากการบ้านที่ 2 ข้อ 1 ให้หาว่าใครเป็นคนสอน และราคาเท่าไร (ห้ามแสดงคอร์สซ้ำ) โดยที่
-- ให้ใช้คำสั่งแค่ statement เดียวเท่านั้น
SELECT
    distinct(c.name) as course_name,
    c.price as courses_price,
    i.name as instructors_name
FROM
    courses as c
LEFT JOIN enrolls as e on c.id = e.course_id 
LEFT JOIN instructors as i on i.id = c.teach_by
ORDER BY c.name;
-- +-------------------------------------+---------------+-----------------------+
-- | course_name                         | courses_price | instructors_name      |
-- +-------------------------------------+---------------+-----------------------+
-- | Acting                              |            90 | Samuel L. Jackson     |
-- | Building a Fashion Brand            |            90 | Diane Von Furstenberg |
-- | Chess                               |            90 | Garry Kasparov        |
-- | Comedy                              |            90 | Steve Martin          |
-- | Conservation                        |            90 | Dr. Jane Goodall      |
-- | Cooking                             |            90 | Wolfgang Puck         |
-- | Cooking #2                          |            90 | Gordon Ramsay         |
-- | Country Music                       |            90 | Reba Mcentire         |
-- | Database System Concept             |            30 | NULL                  |
-- | Design and Architecture             |            90 | Frank Gehry           |
-- | Dramatic Writing                    |            90 | David Mamet           |
-- | Electronic Music Production         |            90 | Deadmau5              |
-- | Fashion Design                      |            90 | Marc Jacobs           |
-- | Film Scoring                        |            90 | Hans Zimmer           |
-- | Filmmaking                          |            90 | Werner Herzog         |
-- | JavaScript for Beginner             |            20 | NULL                  |
-- | Jazz                                |            90 | Herbie Hancock        |
-- | OWASP Top 10                        |            75 | NULL                  |
-- | Photography                         |            90 | Annie Leibovitz       |
-- | Screenwriting                       |            90 | Aaron Sorkin          |
-- | Shooting, Ball Handler, and Scoring |            90 | Stephen Curry         |
-- | Singing                             |            90 | Christina Aguilera    |
-- | Tennis                              |            90 | Serena Williams       |
-- | The Art of Performance              |            90 | Usher                 |
-- | Writing                             |            90 | Judy Blume            |
-- | Writing #2                          |            90 | James Patterson       |
-- | Writing for Television              |            90 | Shonda Rhimes         |
-- +-------------------------------------+---------------+-----------------------+
-- 27 rows in set (0.00 sec)

-- จากการบ้านที่ 2 ข้อ 2 ให้หาว่าใครเป็นคนสอน และราคาเท่าไร (ห้ามแสดงคอร์สซ้ำ) โดยที่
-- ให้ใช้คำสั่งแค่ statement เดียวเท่านั้น
SELECT
    distinct(c.name) as course_name,
    c.price as courses_price,
    i.name as instructors_name
FROM
    courses as c
LEFT JOIN enrolls as e on c.id = e.course_id 
LEFT JOIN instructors as i on i.id = c.teach_by
WHERE e.course_id IS NULL
ORDER BY c.name;
-- +------------------------+---------------+------------------+
-- | course_name            | courses_price | instructors_name |
-- +------------------------+---------------+------------------+
-- | Dramatic Writing       |            90 | David Mamet      |
-- | OWASP Top 10           |            75 | NULL             |
-- | Photography            |            90 | Annie Leibovitz  |
-- | The Art of Performance |            90 | Usher            |
-- | Writing for Television |            90 | Shonda Rhimes    |
-- +------------------------+---------------+------------------+
-- 5 rows in set (0.00 sec)

SELECT
    distinct(c.name) as course_name,
    c.price as courses_price,
    i.name as instructors_name
FROM
    courses as c
LEFT JOIN enrolls as e on c.id = e.course_id 
LEFT JOIN instructors as i on i.id = c.teach_by
WHERE e.course_id IS NULL AND i.name IS NOT NULL
ORDER BY c.name;

-- +------------------------+---------------+------------------+
-- | course_name            | courses_price | instructors_name |
-- +------------------------+---------------+------------------+
-- | Dramatic Writing       |            90 | David Mamet      |
-- | Photography            |            90 | Annie Leibovitz  |
-- | The Art of Performance |            90 | Usher            |
-- | Writing for Television |            90 | Shonda Rhimes    |
-- +------------------------+---------------+------------------+
-- 4 rows in set (0.00 sec)