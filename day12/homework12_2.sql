use lab12;
-- เพิ่ม student ลงไป 10 คน
-- clear all DATA
SET
    FOREIGN_KEY_CHECKS = 0;
truncate table students;
SET
    FOREIGN_KEY_CHECKS = 1;
-- reset auto increments
ALTER TABLE
    students auto_increment = 1;
insert into
    students (name)
values
    ('Chairat Janpong'),
    ('Mok Udomprecha'),
    ('Somdet-ong-yai Nakpradith'),
    ('Komn Udomprecha'),
    ('Nakarin Pongchandaj'),
    ('Sarut Mahidol'),
    ('Prachuab Noppachorn'),
    ('Thampon Dajpaisarn'),
    ('Chao-Khun-Sa Vanich'),
    ('Thong Di Chutimant');

-- ให้ student ลงเรียนแต่ละวิชาที่แตกต่างกัน (อาจจะซ้ำกันบ้าง)
insert into
    enrolls (student_id, course_id)
values
    (1, 10),
    (1, 12),
    (1, 13),
    (1, 15),
    (1, 16),
    (1, 18),
    (1, 21),
    (1, 22),
    (1, 25),
    (1, 6),
    (1, 8),
    (1, 9),
    (2, 1),
    (2, 10),
    (2, 11),
    (2, 13),
    (2, 14),
    (2, 15),
    (2, 16),
    (2, 18),
    (2, 21),
    (2, 25),
    (2, 3),
    (2, 4),
    (2, 6),
    (2, 9),
    (3, 1),
    (3, 10),
    (3, 11),
    (3, 12),
    (3, 13),
    (3, 2),
    (3, 20),
    (3, 26),
    (3, 4),
    (3, 5),
    (3, 6),
    (4, 11),
    (4, 12),
    (4, 15),
    (4, 18),
    (4, 23),
    (4, 25),
    (4, 26),
    (4, 5),
    (4, 8),
    (4, 9),
    (5, 1),
    (5, 10),
    (5, 12),
    (5, 15),
    (5, 2),
    (5, 20),
    (5, 23),
    (5, 25),
    (5, 6),
    (5, 8),
    (5, 9),
    (6, 1),
    (6, 13),
    (6, 14),
    (6, 20),
    (6, 25),
    (6, 26),
    (6, 4),
    (6, 5),
    (6, 8),
    (7, 1),
    (7, 11),
    (7, 12),
    (7, 15),
    (7, 16),
    (7, 2),
    (7, 20),
    (7, 21),
    (7, 22),
    (7, 26),
    (7, 4),
    (7, 6),
    (8, 10),
    (8, 11),
    (8, 12),
    (8, 14),
    (8, 16),
    (8, 18),
    (8, 23),
    (8, 5),
    (8, 6),
    (8, 9),
    (9, 1),
    (9, 13),
    (9, 14),
    (9, 16),
    (9, 2),
    (9, 22),
    (9, 23),
    (9, 25),
    (9, 3),
    (9, 4),
    (9, 6),
    (10, 1);

-- มีคอร์สไหนบ้างที่มีคนเรียน (ห้ามแสดงชื่อคอร์สซ้ำ)
SELECT
    distinct(c.name) as course_name
FROM
    enrolls as e
LEFT JOIN courses as c on c.id = e.course_id;
-- +-------------------------------------+
-- | course_name                         |
-- +-------------------------------------+
-- | Cooking                             |
-- | Acting                              |
-- | Chess                               |
-- | Writing                             |
-- | Conservation                        |
-- | Tennis                              |
-- | Writing #2                          |
-- | Building a Fashion Brand            |
-- | Design and Architecture             |
-- | Singing                             |
-- | Jazz                                |
-- | Country Music                       |
-- | Fashion Design                      |
-- | Film Scoring                        |
-- | Comedy                              |
-- | Filmmaking                          |
-- | Screenwriting                       |
-- | Electronic Music Production         |
-- | Cooking #2                          |
-- | Shooting, Ball Handler, and Scoring |
-- | Database System Concept             |
-- | JavaScript for Beginner             |
-- +-------------------------------------+
-- 22 rows in set (0.00 sec)

-- มีคอร์สไหนบ้างที่ไม่มีคนเรียน
SELECT
    distinct(c.name) as course_name
FROM
    courses as c
LEFT JOIN enrolls as e on c.id = e.course_id 
WHERE e.course_id IS NULL;
-- +------------------------+
-- | course_name            |
-- +------------------------+
-- | Dramatic Writing       |
-- | OWASP Top 10           |
-- | Photography            |
-- | The Art of Performance |
-- | Writing for Television |
-- +------------------------+
-- 5 rows in set (0.02 sec)