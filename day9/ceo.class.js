const fs = require("fs");
const { Employee } = require("./employee.class");
const { OfficeCleaner } = require("./officecleaner.class");
const { Programmer } = require("./programmer.class");

class CEO extends Employee {
  constructor(firstName, lastName, salary, id, dressCode) {
    super(firstName, lastName, salary);
    this.id = id;
    this.dressCode = "suit";
    this.employeeRaw = [];
    this.employee = [];
  }

  async getEmployees() {
    try {
      let rawData = await this._getDataFromFile("./homework9.json");
      this.employeeRaw = rawData;
      let result = await this._getEmployee(rawData);
      this.employee = result;
      return result;
    } catch (err) {
      console.error(err);
    }
  }

  _getDataFromFile(fileName) {
    return new Promise((resolve, reject) => {
      fs.readFile(fileName, "utf8", function(err, data) {
        if (err) {
          reject(err);
        } else {
          resolve(JSON.parse(data));
        }
      });
    });
  }

  _getEmployee(rawData) {
    return new Promise((resolve, reject) => {
      let returnData = rawData.map(function(person) {
        let role = person.role;
        let objPerson;
        if (role === "CEO") {
          objPerson = new CEO(
            person.firstname,
            person.lastname,
            person.salary,
            person.id,
            person.dressCode
          );
        }
        if (role === "Programmer") {
          objPerson = new Programmer(
            person.id,
            person.firstname,
            person.lastname,
            person.salary,
            person.type
          );
        }
        if (role === "OfficeCleaner") {
          objPerson = new OfficeCleaner(
            person.firstname,
            person.lastname,
            person.salary,
            person.id,
            person.dressCode
          );
        }
        return objPerson;
      });
      if (returnData.lenght == 0) {
        reject("empty data ...error");
      } else {
        resolve(returnData);
      }
    });
  }

  getSalary() {
    return super.getSalary() * 2;
  }

  work(employee) {
    // simulate public method
    this._fire(employee);
    this._hire(employee);
    this._seminar();
    this._golf();
  }

  increaseSalary(employee, newSalary) {
    let employeeSalary = employee.getSalary();
    if (employeeSalary > newSalary) {
      console.log(employee.firstName + "'s salary is less than before!!");
    } else {
      this.setSalary(newSalary);
      console.log(
        employee.firstName + "'s salary has been set to " + newSalary
      );
    }
  }

  gossip(employee, context) {
    console.log("Hey " + employee.firstName + ", " + context);
  }

  _fire(employee) {
    console.log(
      employee.firstName + " has been fired! Dress with :" + employee.dressCode
    );
  }

  _hire(employee) {
    console.log(
      employee.firstName +
        " has been hired back! Dress with :" +
        employee.dressCode
    );
  }

  _seminar() {
    console.log("He is going to seminar Dress with :" + this.dressCode);
  }

  _golf() {
    // simulate private method
    this.dressCode = "golf_dress";
    console.log(
      "He goes to golf club to find a new connection." +
        " Dress with :" +
        this.dressCode
    );
  }

  talk(robotMessage) {
    console.log(robotMessage);
  }

  reportRobot(seft, robotMessage) {
    console.log('\nreportRobot from ' + seft.firstName + ' to CEO');
    seft.talk(robotMessage);
  }
}

exports.CEO = CEO;
