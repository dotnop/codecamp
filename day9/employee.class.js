const fs = require("fs");

class Employee {
  constructor(firstName, lastName, salary) {
    this._salary = salary; // simulate private variable
    this.firstName = firstName;
    this.dressCode = "tshirt";
  }
  setSalary(newSalary) {
    this._salary = newSalary;
  }
  getSalary() {
    // simulate public method
    return this._salary;
  }
  work(employee) {
    // leave blank for child class to be overidden
  }

  leaveForVacation(year, month, day) {}
}
exports.Employee = Employee;
