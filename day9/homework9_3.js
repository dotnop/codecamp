const _ = require("lodash");

class MyUtility {
  assign() {
    return _.assign(Array.from(arguments));
  }

  times() {
    return _.times(Array.from(arguments));
  }

  keyBy() {
    return _.keyBy(Array.from(arguments));
  }

  cloneDeep() {
    return _.cloneDeep(Array.from(arguments));
  }

  filter() {
    return _.filter(Array.from(arguments));
  }

  sortBy() {
    return _.sortBy(Array.from(arguments));
  }
}

let o = new MyUtility();
let t = o.times(5);
console.log(t);
