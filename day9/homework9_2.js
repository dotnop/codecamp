const { OfficeCleaner } = require("./officecleaner.class");
const { CEO } = require("./ceo.class");

let gb = new OfficeCleaner("gb", "lastname", 30000, 1001, "maid");
let Somchai = new CEO("Somchai", "lastname", 30000, 1001, "tshirt");
// console.log(gb);
// console.log(Somchai);

Somchai.getEmployees().then(showData => {
  Somchai.employee.map(person => {
    console.log(person);
    console.log(person.__proto__.constructor.name + ' start working...');
    person.work(gb);
    console.log('\n');
  });
});
