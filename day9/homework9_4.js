const fs = require("fs");
const Events = require("events");
const EventEmitter = new Events();
const { CEO } = require("./ceo.class");

function runFirst() {
  return new Promise((resolve, reject) => {
    let Somchai = new CEO("Somchai", "Sudlor", 30000);
    Somchai.getEmployees().then(showData => {
      Somchai.employee.map(person => {
        EventEmitter.on("writeRobotFinish", (self, message) =>
          person.reportRobot(self, message)
        );
      });
      resolve(Somchai);
    });
  });
}

function runSecond(Somchai) {
  return new Promise((resolve, reject) => {
    fs.readFile("head.txt", "utf8", function(err, data) {
      let head = data;

      fs.readFile("body.txt", "utf8", function(err, data) {
        let body = data;

        fs.readFile("leg.txt", "utf8", function(err, data) {
          let leg = data;

          fs.readFile("feet.txt", "utf8", function(err, data) {
            let feet = data;
            let robotMessage =
              head + "\r\n" + body + "\r\n" + leg + "\r\n" + feet;

            fs.writeFile("robot.txt", robotMessage, "utf8", function(err) {
              if (err) reject(err);
              else {
                EventEmitter.emit("writeRobotFinish", Somchai, robotMessage);
                resolve(true);
              }
            });
          });
        });
      });
    });
  });
}

async function runAll() {
  let Somchai = await runFirst();
  await runSecond(Somchai);
}

runAll();
