const { Employee } = require("./employee.class");

class Programmer extends Employee {
  constructor(id, firstName, lastName, salary,type) {
    super(firstName, lastName, salary);
    this.id = id;
    this.type = type;
  }

  work() {
    this.CreateWebsite();
    this.FixPC();
    this.InstallWindows();
  }

  CreateWebsite() {
    console.log("CreateWebsite");
  }

  FixPC() {
    console.log("FixPC");
  }

  InstallWindows() {
    console.log("InstallWindows");
  }

  leaveForVacation() {
    console.log("leaveForVacation");
  }

  gossip() {
    console.log("gossip");
  }

  talk(robotMessage) {
    console.log(robotMessage);
  }

  reportRobot(seft, robotMessage) {
    console.log('\nreportRobot from ' + seft.firstName + ' to programmer');
    seft.talk(robotMessage);
  }
}

exports.Programmer = Programmer;
