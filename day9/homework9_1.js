const fs = require("fs");
const Events = require("events");
const EventEmitter = new Events();
const { CEO } = require("./ceo.class");

let Somchai = new CEO("Somchai", "Sudlor", 30000);

EventEmitter.on("writeRobotFinish", Somchai.reportRobot);

fs.readFile("head.txt", "utf8", function(err, data) {
  let head = data;
  fs.readFile("body.txt", "utf8", function(err, data) {
    let body = data;
    fs.readFile("leg.txt", "utf8", function(err, data) {
      let leg = data;
      fs.readFile("feet.txt", "utf8", function(err, data) {
        let feet = data;
        let robotMessage = head + "\r\n" + body + "\r\n" + leg + "\r\n" + feet;
        fs.writeFile("robot.txt", robotMessage, "utf8", function(err) {
          if (err) console.error(err);
          else EventEmitter.emit("writeRobotFinish", Somchai, robotMessage);
        });
      });
    });
  });
});
