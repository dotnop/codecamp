const { Employee } = require("./employee.class");

class OfficeCleaner extends Employee {
  constructor(firstName, lastName, salary, id, dressCode) {
    super(firstName, lastName, salary);
    this.id = id;
    this.dressCode = dressCode;
  }

  work() {
    this.Clean();
    this.KillCoachroach();
    this.DecorateRoom();
    this.WelcomeGuest();
  }

  Clean() {
    console.log("Clean");
  }

  KillCoachroach() {
    console.log("KillCoachroach");
  }

  DecorateRoom() {
    console.log("DecorateRoom");
  }

  WelcomeGuest() {
    console.log("WelcomeGuest");
  }

  leaveForVacation() {
    console.log("leaveForVacation");
  }

  gossip() {
    console.log("gossip");
  }

  talk(robotMessage) {
    console.log(robotMessage);
  }

  reportRobot(seft, robotMessage) {
    console.log(
      "\nreportRobot from " +
        seft.firstName +
        " to Office Cleaner" +
        this.firstName
    );
    seft.talk(robotMessage);
  }
}

exports.OfficeCleaner = OfficeCleaner;
