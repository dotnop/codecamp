const fs = require("fs");

let headTxt = new Promise(function(resolve, reject) {
  fs.readFile("./head.txt", "utf8", function(err, data) {
    if (err) {
      reject(err);
    } else {
      data = data + "\r\n";
      resolve(data);
    }
  });
});

let bodyTxt = new Promise(function(resolve, reject) {
  fs.readFile("./body.txt", "utf8", function(err, data) {
    if (err) {
      reject(err);
    } else {
      data = data + "\r\n";
      resolve(data);
    }
  });
});

let legTxt = new Promise(function(resolve, reject) {
  fs.readFile("./leg.txt", "utf8", function(err, data) {
    if (err) {
      reject(err);
    } else {
      data = data + "\r\n";
      resolve(data);
    }
  });
});

let feetTxt = new Promise(function(resolve, reject) {
  fs.readFile("./feet.txt", "utf8", function(err, data) {
    if (err) {
      reject(err);
    } else {
      data = data + "\r\n";
      resolve(data);
    }
  });
});

Promise.all([headTxt, bodyTxt, legTxt, feetTxt])
  .then(function(result) {
    let writeData = "";
    for (let i in result) {
      writeData += result[i];
    }
    fs.writeFile("./robot.txt", writeData, "utf8");
  })
  .catch(function(err) {
    console.error("error some promise");
  });
