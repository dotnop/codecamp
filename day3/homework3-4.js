let fs = require("fs");

let socialNewData = [];
let filterKey = ["name", "gender", "company", "email", "friends"];
let info;
fs.readFile(__dirname + "/homework1-4.json", "utf8", (err, data) => {
  if (err) {
    console.error(err);
  } else {
    info = JSON.parse(data);
    info.forEach((personData, index) => {
      let personNewData = {};
      Object.keys(personData).forEach(key => {
        if (filterKey.includes(key)) {
          personNewData[key] = personData[key];
        }
      });
      socialNewData[index] = personNewData;
    });
  }
  console.log(socialNewData);
  fs.writeFile(
    "socialnewdata.json",
    JSON.stringify(socialNewData),
    "utf8",
    err => {
      if (err) console.error(err);
      else console.log("write file : socialnewdata.json ");
    }
  );
});
