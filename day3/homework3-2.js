const fs = require("fs");

function myReadFile(fileName) {
  return new Promise(function(resolve, reject) {
    fs.readFile(fileName, "utf8", function(err, data) {
      if (err) {
        reject(err);
      } else {
        data = data + "\r\n";
        resolve(data);
      }
    });
  });
}

function myWriteFile(fileName, writeDataTxt) {
  return new Promise(function(resolve, reject) {
    fs.writeFile(fileName, writeDataTxt, "utf8", function(err) {
      if (err) {
        reject(err);
      } else {
        resolve("success write file");
      }
    });
  });
}

async function doReadWriteFile() {
  try {
    let headTxt = await myReadFile("head.txt");
    let bodyTxt = await myReadFile("body.txt");
    let legTxt = await myReadFile("leg.txt");
    let feetTxt = await myReadFile("feet.txt");
    let robotTxt = headTxt + bodyTxt + legTxt + feetTxt;
    await myWriteFile("robot.txt", robotTxt);
  } catch (error) {
    console.error("error");
  }
}

doReadWriteFile();
