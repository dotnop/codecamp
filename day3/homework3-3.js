const fs = require("fs");

let employees = [
  { id: "1001", firstname: "Luke", lastname: "Skywalker" },
  { id: "1002", firstname: "Tony", lastname: "Stark" },
  { id: "1003", firstname: "Somchai", lastname: "Jaidee" },
  { id: "1004", firstname: "Monkey D", lastname: "Luffee" }
];
let company = [
  { id: "1001", company: "Walt Disney" },
  { id: "1002", company: "Marvel" },
  { id: "1003", company: "Love2work" },
  { id: "1004", company: "One Piece" }
];
let salaries = [
  { id: "1001", salary: "40000" },
  { id: "1002", salary: "1000000" },
  { id: "1003", salary: "20000" },
  { id: "1004", salary: "9000000" }
];
let like = [
  { id: "1001", like: "apple" },
  { id: "1002", like: "banana" },
  { id: "1003", like: "orange" },
  { id: "1004", like: "papaya" }
];
let dislike = [
  { id: "1001", dislike: "banana" },
  { id: "1002", dislike: "orange" },
  { id: "1003", dislike: "papaya" },
  { id: "1004", dislike: "apple" }
];

let employeesDataBase = [];
let personObj = {};
let currentId = 0;
for (let i = 0; i < employees.length; i++) {
  personObj[employees[i].id] = {};
  for (let key in employees[i]) {
    personObj[employees[i].id][key] = employees[i][key];
  }
}
for (let i = 0; i < company.length; i++) {
  for (let key in company[i]) {
    personObj[company[i].id][key] = company[i][key];
  }
}
for (let i = 0; i < salaries.length; i++) {
  for (let key in salaries[i]) {
    personObj[salaries[i].id][key] = salaries[i][key];
  }
}
for (let i = 0; i < like.length; i++) {
  for (let key in like[i]) {
    personObj[like[i].id][key] = like[i][key];
  }
}
for (let i = 0; i < dislike.length; i++) {
  for (let key in dislike[i]) {
    personObj[dislike[i].id][key] = dislike[i][key];
  }
}
for (let i in personObj) {
  employeesDataBase.push(personObj[i]);
}

fs.writeFile(
  "homework3-3.json",
  JSON.stringify(employeesDataBase),
  "utf8",
  function(err) {
    if (err) console.error(err);
  }
);
