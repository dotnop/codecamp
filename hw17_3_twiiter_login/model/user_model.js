const mysql = require('mysql2/promise');

const pool = mysql.createPool({
  connectionLimit: 10,
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'nottwitter',
});

module.exports = {
  async insertNewUser(username, password, email) {
    let sql = `INSERT INTO users
        (username,password,email)
        VALUES
        (?,?,?)
        `;
    let [result, fields] = await pool.query(sql, [
      username,
      this.stupidHash(password),
      email,
    ]);
    return result.insertId;
  },
  stupidHash(password) {
    return password + '12345';
  },
  async isEmailExisted(email) {
    let sql = `SELECT email FROM users
        WHERE email = ?`;
    let [result, fields] = await pool.query(sql, [email]);
    if (result[0]) return true;
    else return false;
  },
  async isUsernameExisted(username) {
    let sql = `SELECT username FROM users
        WHERE username = ?`;
    let [result, fields] = await pool.query(sql, [username]);
    if (result[0]) return true;
    else return false;
  },
  async isPasswordMatched(username, password) {
    let sql = `SELECT password FROM users WHERE username = ?`;
    let [result, fields] = await pool.query(sql, [username]);
    if (result[0].password == this.stupidHash(password)) return true;
    else return false;
  },
  async getUserInfoByUserId(userId) {
    let sql = `SELECT * FROM users WHERE id=?`;
    let [result, fields] = await pool.query(sql, [userId]);
    return result[0];
  },
  async getUserInfoByUsername(username) {
    let sql = `SELECT * FROM users WHERE username=?`;
    let [result, fields] = await pool.query(sql, [username]);
    return result[0];
  },
};
