const userModel = require('../model/user_model');
const baseUrl = 'http://localhost:3000/';
module.exports = {
  async register(ctx) {
    await ctx.render('register', {
      baseUrl: baseUrl,
    });
  },
  async registerCompleted(ctx) {
    let emailExisted = false;
    let usernameExisted = false;
    let emailExistedResult = await userModel.isEmailExisted(
      ctx.request.body.email
    );
    if (emailExistedResult) {
      emailExisted = true;
    }

    let usernameExistedResult = await userModel.isUsernameExisted(
      ctx.request.body.username
    );
    if (usernameExistedResult) {
      usernameExisted = true;
    }

    let userId;
    if (!emailExisted && !usernameExisted) {
      userId = await userModel.insertNewUser(
        ctx.request.body.username,
        ctx.request.body.password,
        ctx.request.body.email
      );
    }

    //console.log(result);
    await ctx.render('register_completed', {
      username: ctx.request.body.username,
      email: ctx.request.body.email,
      emailExisted: emailExisted,
      usernameExisted: usernameExisted,
      userId: userId,
    });
  },
  async registerCompletedAjax(ctx) {
    let emailExisted = false;
    let usernameExisted = false;
    let emailExistedResult = await userModel.isEmailExisted(
      ctx.request.body.email
    );
    if (emailExistedResult) emailExisted = true;

    let usernameExistedResult = await userModel.isUsernameExisted(
      ctx.request.body.username
    );
    if (usernameExistedResult) usernameExisted = true;

    let userId;
    if (!emailExisted && !usernameExisted)
      userId = await userModel.insertNewUser(
        ctx.request.body.username,
        ctx.request.body.password,
        ctx.request.body.email
      );

    ctx.body = {
      emailExisted: emailExisted,
      usernameExisted: usernameExisted,
    };
  },
  async login(ctx, next) {
    // if (!ctx.session || (ctx.session && !ctx.session.userId)) {
    await ctx.render('login');
    // } else {
    //   await next();
    // }
  },
  async loginVerify(ctx) {
    let usernameExisted = false;
    let passwordMatched = false;

    let usernameExistedResult = await userModel.isUsernameExisted(
      ctx.request.body.username
    );
    if (usernameExistedResult) {
      usernameExisted = true;
      if (
        await userModel.isPasswordMatched(
          ctx.request.body.username,
          ctx.request.body.password
        )
      ) {
        passwordMatched = true;
      }
    }
    if (passwordMatched) {
      let userRow = await userModel.getUserInfoByUsername(
        ctx.request.body.username
      );
      ctx.session.userId = userRow.id;
      await ctx.render('login_completed', {
        username: ctx.request.body.username,
      });
    } else {
      await ctx.render('login_failed', {
        usernameExisted: false,
        passwordMatched: false,
      });
    }
  },
  async myProfile(ctx) {
    if (!ctx.session || (ctx.session && !ctx.session.userId)) {
      console.log(ctx.session);
      await ctx.render('notallow');
    } else {
      const userRow = await userModel.getUserInfoByUserId(ctx.session.userId);
      await ctx.render('user_profile', {
        userRow: userRow,
      });
    }
  },
  async logout(ctx) {
    console.log('logout');
    ctx.session = null;
    ctx.body = 'Logged out!!';
  },
  async admin(ctx) {
    await ctx.render('admin');
  },
};
