const userModel = require('../model/user_model');
const baseUrl = 'http://localhost:3000/';
module.exports = {
  async homepage(ctx) {
    let userRow = {
      name: 'anonymous',
      logined: false,
    };
    if (ctx.session.userId) {
      userRow = await userModel.getUserInfoByUserId(ctx.session.userId);
      logined = true;
    }
    await ctx.render('homepage', {
      name: userRow.name,
      logined: userRow.logined,
    });
  },
  async auth(ctx, next) {
    console.log('auth');
    if (
      !ctx.session.userId ||
      ctx.path === '/auth/signup' ||
      ctx.path === '/auth/signin' ||
      ctx.path === '/'
    ) {
      ctx.status = 401;
      ctx.body = 'Forbidden error!';
      await next();
    } else {
      // await next();
    }
  },
};
