const Koa = require('Koa');
const Router = require('koa-router');
const session = require('koa-session');
const render = require('koa-ejs');
const path = require('path');
const app = new Koa();
const router = new Router();
const formidable = require('koa-bodyparser');
const userController = require('./controller/user');
const utilController = require('./controller/util');
const userModel = require('./model/user_model');
const locahostPort = 3000;

const baseUrl = 'http://localhost:3000/';

render(app, {
  root: path.join(__dirname, 'view'),
  layout: 'template',
  viewExt: 'ejs',
  cache: false,
  debug: false,
});

app.keys = ['what-you-do-today-is-your-future'];

//// .get('/register', userController.register)
//// .post('/register_completed', userController.registerCompleted)
//// .post('/register_completed_ajax', userController.registerCompletedAjax)
//// .get('/login', userController.login)
//// .post('/login_completed', userController.loginCompleted)
// .get('/my_profile', userController.myProfile)
//// .get('/logout', userController.logout)
// .get('/admin', userController.admin);

router
  .get('/', utilController.homepage)
  .get('/auth/signup', userController.register)
  //.get('/login_completed', userController.registerCompleted)
  .post('/auth/signup', userController.registerCompleted)
  .post('/auth/signup_ajax', userController.registerCompletedAjax)
  .get('/auth/signin', userController.login)
  .post('/auth/signin', userController.loginVerify)
  .get('/login_completed', utilController.homepage)
  .get('/auth/signout', userController.logout)
  .post('/auth/verify', ctx => {
    ctx.body = 'verify';
  })

  // ## Upload
  .post('/upload', ctx => {
    ctx.body = 'upload';
  })

  // ## User
  .get('/my_profile', userController.myProfile)
  .patch('/user/:id', ctx => {
    ctx.body = 'id';
  })
  .put('/user/:id/follow', ctx => {
    ctx.body = 'follow';
  })
  .delete('/user/:id/follow', ctx => {
    ctx.body = 'follow';
  })
  .get('/user/:id/follow', ctx => {
    ctx.body = '/user/:id/follow';
  })
  .get('/user/:id/followed', ctx => {
    ctx.body = '/user/:id/followed';
  })

  // ## Tweet
  .get('/tweet', ctx => {
    ctx.body = '/tweet';
  })
  .post('/tweet', ctx => {
    ctx.body = '/tweet';
  })
  .put('/tweet/:id/like', ctx => {
    ctx.body = '/tweet/:id/like';
  })
  .delete('/tweet/:id/like', ctx => {
    ctx.body = '/tweet/:id/like';
  })
  .post('/tweet/:id/retweet', ctx => {
    ctx.body = '/tweet/:id/retweet';
  })
  .put('/tweet/:id/vote/:voteId', ctx => {
    ctx.body = '/tweet/:id/vote/:voteId';
  })
  .post('/tweet/:id/reply', ctx => {
    ctx.body = '/tweet/:id/reply';
  })

  // ## Notification
  .get('/notification', ctx => {
    ctx.body = '/notification';
  })

  // ## Direct Message
  .get('/message', ctx => {
    ctx.body = '/message';
  })
  .get('/message/:userId', ctx => {
    ctx.body = '/message/:userId';
  })
  .post('/message/:userId', ctx => {
    ctx.body = '/message/:userId';
  });

const sessionStore = {};

const CONFIG = {
  key: 'koa:sess' /** (string) cookie key (default is koa:sess) */,
  /** (number || 'session') maxAge in ms (default is 1 days) */
  /** 'session' will result in a cookie that expires when session/browser is closed */
  /** Warning: If a session cookie is stolen, this cookie will never expire */
  maxAge: 60 * 60 * 1000,
  overwrite: true /** (boolean) can overwrite or not (default true) */,
  httpOnly: true /** (boolean) httpOnly or not (default true) */,
  store: {
    get(key, maxAge, { rolling }) {
      return sessionStore[key];
    },
    set(key, sess, maxAge, { rolling, changed }) {
      sessionStore[key] = sess;
    },
    destroy(key) {},
  },
};

app
  .use(formidable())
  .use(session(CONFIG, app))
  .use(async (ctx, next) => {
    console.log('ctx.path:', ctx.path);
    console.log('ctx.session:', ctx.session);
    console.log('sessionStore:', sessionStore);
    await next();
  })
  .use(async (ctx, next) => {
    if (ctx.path == '/auth/signin') {
      await next();
      return;
    }
    if (!ctx.session || (ctx.session && !ctx.session.userId)) {
      await ctx.render('login');
    } else {
      await next();
    }
  })
  .use(router.routes())
  .use(router.allowedMethods())
  .listen(locahostPort, () => {
    console.log(`localhost run on port ${locahostPort}`);
  });
