let fields = ["id", "firstname", "lastname", "company", "salary"];
let employees = [
  ["1001", "Luke", "Skywalker", "Walt Disney", "40000"],
  ["1002", "Tony", "Stark", "Marvel", "1000000"],
  ["1003", "Somchai", "Jaidee", "Love2work", "20000"],
  ["1004", "Monkey D", "Luffee", "One Piece", "9000000"]
];

let newEmployees = [];
for (i = 0; i < employees.length; i++) {
  let person = {};
  for (j = 0; j < fields.length; j++) {
    person[fields[j]] = employees[i][j];
  }
  newEmployees[i] = person;
}

console.log(newEmployees);
