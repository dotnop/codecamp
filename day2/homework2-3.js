const fs = require("fs");
let jsonData = "";
let data = fs.readFileSync("employee.txt", "utf8");

try {
  jsonData = JSON.parse(data);
} catch (error) {
  console.error(error);
}

for (let i = 0; i < jsonData.length; i++) {
  console.log(`${jsonData[i].firstname} ${jsonData[i].lastname}`);
}
