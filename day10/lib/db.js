const { dbConfig } = require("../config/database");

class EmployeeDatabase {
  constructor() {
    this.connection = null;
    this.connectDatabase();
  }

  async connectDatabase() {
    try {
      const mysql = require("mysql2/promise");
      this.connection = await mysql.createConnection({
        host: dbConfig.host,
        user: dbConfig.user,
        password: dbConfig.password,
        database: dbConfig.database
      });
    } catch (error) {
      console.error(error);
    }
  }

  async execute(sql) {
    try {
      if (this.connection === null) {
        await this.connectDatabase();
      }
      const [rows, feilds] = await this.connection.query(sql);
      return rows;
    } catch (error) {
      console.error(error);
    }
  }
}

// let run = async function() {
//   try {
//     let DB = await new EmployeeDatabase();
//     let employeeData = await DB.execute("select * from user");
//     console.log(employeeData);
//   } catch (error) {
//     console.error(error);
//   }
// };

// run();

module.exports.DB = new EmployeeDatabase();
