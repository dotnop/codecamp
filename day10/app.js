const render = require("koa-ejs");
const serve = require("koa-static");
const assetPath = "./public";
const Koa = require("koa");
const app = new Koa();

//static for asset
app.use(serve(assetPath));

//all router
require("./controller/routes.js")(app);

//template
const path = require("path");
render(app, {
  root: path.join(__dirname, "views"),
  layout: "template",
  viewExt: "ejs",
  cache: false,
  debug: true
});

app.listen(3000);
console.log("App listen on port 3000");
