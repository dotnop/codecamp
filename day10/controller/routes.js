const Router = require("koa-router");
const Employee = require("../models/employee");
let employee = new Employee();

module.exports = app => {
  const router = new Router({
    prefix: "/table"
  });
  // responds to "/table"
  router.get("/", async (ctx, next) => {
    try {
      let employeeData = await employee.getAll();
      await ctx.render("homework10_1", {
        data: employeeData
      });
      await next();
    } catch (err) {
      ctx.status = 400;
      ctx.body = err;
    }
  });
  //router.redirect("", "table");
  app.use(router.routes());
  app.use(
    router.allowedMethods({
      throw: true,
      notImplemented: () => new Boom.notImplemented(),
      methodNotAllowed: () => new Boom.methodNotAllowed()
    })
  );
};
